{-# LANGUAGE OverloadedStrings #-}
import Test.Syd
import Test.QuickCheck.Monadic

import Data.Bits
import Data.Word
import Control.Lens
import Control.Monad.State

--import LC100
import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions

import LC100.Test.MathsTests
import LC100.Test.ShiftTests
import LC100.Test.IncrementTests
import LC100.Test.BitTestTests
import LC100.Test.BranchTests
import LC100.Test.JumpTests
import LC100.Test.ProcUtilityTests
import LC100.Test.MemoryTests
import LC100.Test.DeviceTests

main :: IO ()
main = sydTest $ do
  aluTests --MathsTests
  shfTests --ShiftTests
  incrementTests
  bitTestTests
  branchTests
  jumpTests
  procUtilTests
  memoryTests
  --deviceTests doesn't seem to work as expected.
  --Testing in GHCi seems to do the right thing, though.
  --deviceTests
