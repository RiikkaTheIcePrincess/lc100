{-# LANGUAGE OverloadedStrings, BinaryLiterals, FlexibleInstances, LambdaCase #-}
module LC100.Test.ProcUtilityTests where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.ByteString
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Data.ByteString.Lazy as BSL (take, drop, pack)
import Data.Word
import Control.Monad
import Control.Monad.State
import Control.Lens hiding (pre)

import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions
import LC100.Memory
import LC100.Test.Utils

procUtilTests = do
  describe "QC processor/utility tests" $ do
    it "SET"  $ property testSETQuick
    --it "SET exception"  $ property testSETQuickExcept
    --it "SET exception" $ goSETQuickExcept 1 R1 0 0
    it "GET"  $ property testGETQuick
    it "SLP"  $ property testSLPQuick
    it "RTI"  $ property testRTIQuick
    pending "SWI pending some more interrupt machinery" --it "SWI"  $ property testSWIQuick

testSETQuick pcReg gpReg ov newv = monadicIO $ do
  --Ensure we have supervisor mode so we can use SET
  let oldv = if (pcReg `mod` 5) /= 1 then ov else setBit ov 0xF
  let rt = rs0 & (mapGP gpReg) .~ newv
  let pcr = mapPCRi $ fromIntegral (pcReg `mod` 5)
  let ss2 = ss0 { _registers=rt & (mapPC pcr) .~ oldv }
  --res <- testInstr (G2Instr SET gpReg (pcReg `mod` 5)) ss2
  res <- testInstr (G2Instr SET gpReg (pcReg `mod` 5)) ss2
  assert $ res ^. registers . mapPC pcr == newv

{- I cannot figure this bullshit out, apparently.
--It seems like I should just be able to tack a "shouldThrow"
--onto a normal test but NOPE it's an Expectation or a PropertyM what the fuck ever
--and that's just not good enough for somefuckingthing
testSETQuickExcept pcReg gpReg oldv newv =
  (goSETQuickExcept pcReg gpReg oldv newv)
  `shouldThrow` chkUPF

goSETQuickExcept pcReg gpReg oldv newv = do --monadicIO $ do
  let rt = rs0 & (mapGP gpReg) .~ newv
  let pcr = mapPCRi $ fromIntegral (pcReg `mod` 5)
  pre (if pcr == PC then testBit oldv 0xF else False)
  let ss2 = ss0 { _registers=rt & (mapPC pcr) .~ oldv }
  --res <- testInstr (G2Instr SET gpReg (pcReg `mod` 5)) ss2
  res <- testInstr (G2Instr SET gpReg (pcReg `mod` 5)) ss2
  (execStateT (execute $ G2Instr SET gpReg (pcReg `mod` 5)) ss2) `shouldThrow` chkUPF
  --assert $ res ^. registers . mapPC pcr == newv
  --res <- execStateT (execute $ G2Instr SET gpReg (pcReg `mod` 5)) ss2
  --iop >>= (\s -> assert $ s ^. registers . mapPC pcr == newv)

chkUPF = \case
  UnprivFault -> True
  otherwise -> False
-}

testGETQuick pcReg gpReg pcv = monadicIO $ do
  let pcr = mapPCRi $ fromIntegral (pcReg `mod` 5)
  let ss2 = ss0 { _registers=rs0 & (mapPC pcr) .~ pcv }
  res <- testInstr (G2Instr GET gpReg (pcReg `mod` 5)) ss2
  assert $ res ^. registers . mapGP gpReg == pcv

testSLPQuick = monadicIO $ do
  res <- testInstr (G2Instr SLP R0 0) ss0
  assert $ testBit (res ^. registers . ps) 0xA

--Weird naming...
--newPS and newPC are the ones to change to; actually the old ones that were stored
testRTIQuick rPS newPS newPC randPC = monadicIO $ do
  let randPS = setBit rPS 0xF
  let rt = rs0 & isr .~ newPS
  let rt' = rt & irpc .~ newPC
  let rt'' = rt' & ps .~ randPS
  let ss2 = ss0 { _registers=rt'' & pc .~ randPC }
  res <- testInstr (G2Instr RTI R0 0) ss2
  assert $ res ^. registers . pc == newPC
  assert $ res ^. registers . ps == newPS

testSWIQuick pcReg gpReg oldv newv = pending "SWI pending some more interrupt machinery"
