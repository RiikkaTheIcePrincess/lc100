{-# LANGUAGE OverloadedStrings, BinaryLiterals, FlexibleInstances #-}
module LC100.Test.BranchTests where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.ByteString
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Data.ByteString.Lazy as BSL (take, drop, pack)
import Data.Word
import Control.Monad.State
import Control.Lens hiding (pre)

import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions
import LC100.Memory
import LC100.Test.Utils

branchTests = do
  describe "QC branch tests" $ do
    it "BNE"  $ property testBNEQuick
    it "BLT"  $ property testBLTQuick
    it "BLTU" $ property testBLTUQuick

testBNEQuick reg0 r0v reg1 r1v offs oldPC = monadicIO $ do
  pre (reg0 /= reg1)
  let rt = rs0 & (mapTReg reg0) .~ r0v
  --We'll set a random initial value to add to
  let rt' = rt & pc .~ oldPC
  let ss2 = ss0 { _registers=rt' & (mapTReg reg1) .~ r1v }
  res <- testInstr (BNCInstr BNE reg0 reg1 offs) ss2
  let sum = oldPC + offs
  assert $ res ^. registers . pc == if r0v /= r1v then sum else oldPC

testBLTQuick reg0 r0v reg1 r1v offs oldPC = monadicIO $ do
  pre (reg0 /= reg1)
  let rt = rs0 & (mapTReg reg0) .~ r0v
  let rt' = rt & pc .~ oldPC
  let ss2 = ss0 { _registers=rt' & (mapTReg reg1) .~ r1v }
  res <- testInstr (BNCInstr BLT reg0 reg1 offs) ss2
  let sum = fromIntegral oldPC + (fromIntegral offs :: Int16)
  assert $ res ^. registers . pc ==
    if (fromIntegral r0v :: Int16) < (fromIntegral r1v :: Int16)
    then fromIntegral sum
    else oldPC
  
testBLTUQuick reg0 r0v reg1 r1v offs oldPC = monadicIO $ do
  pre (reg0 /= reg1)
  let rt = rs0 & (mapTReg reg0) .~ r0v
  let rt' = rt & pc .~ oldPC
  let ss2 = ss0 { _registers=rt' & (mapTReg reg1) .~ r1v }
  res <- testInstr (BNCInstr BLTU reg0 reg1 offs) ss2
  let sum = oldPC + offs
  assert $ res ^. registers . pc ==
    if r0v < r1v
    then sum
    else oldPC
