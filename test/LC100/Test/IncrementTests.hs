{-# LANGUAGE OverloadedStrings, BinaryLiterals, FlexibleInstances #-}
module LC100.Test.IncrementTests where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.ByteString
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Data.ByteString.Lazy as BSL (take, drop, pack)
import Data.Word
import Control.Monad.State
import Control.Lens hiding (pre)

import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions
import LC100.Memory
import LC100.Test.Utils

incrementTests = do
  describe "QC increment tests" $ do
    it "ADI" $ property testADIQuick
    it "SBI" $ property testSBIQuick

testADIQuick reg oldVal opd = monadicIO $ do
  --We'll set a random initial value to add to
  let ss2 = ss0 { _registers=rs0 & (mapGP reg) .~ oldVal }
  res <- testInstr (IMMInstr ADI reg opd) ss2
  let sum = ss2 ^. registers . mapGP reg + opd
  assert $ res ^. registers . mapGP reg == if reg == R0 then 0 else sum

testSBIQuick reg oldVal opd = monadicIO $ do
  --We'll set a random initial value to add to
  let ss2 = ss0 { _registers=rs0 & (mapGP reg) .~ oldVal }
  res <- testInstr (IMMInstr SBI reg opd) ss2
  let sum = ss2 ^. registers . mapGP reg - opd
  assert $ res ^. registers . mapGP reg == if reg == R0 then 0 else sum
