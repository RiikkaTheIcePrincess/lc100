{-# LANGUAGE BinaryLiterals #-}
module LC100.Test.Utils where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.ByteString
import Control.Lens hiding (pre)
import Control.Monad.State
import Data.ByteString.Lazy as BSL
import LC100.Instructions
import LC100.SystemState
import LC100.Misc

ss0 = initialSystemState
rs1 = rs0{_r1=1,_r2=2,_r3=3,_r4=4,_r5=5,_r6=6,_r7=7,_r8=7} 
ss1 = ss0 { _memory = pack [0,1,2,3,4,5,6,7,8,9,10],
            _registers=rs1}
ss1_16 = ss0 { _memory = pack [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0,8,0,9,0,10,0],
            _registers=rs1}
ssPaged = set memory (testPages) ss0paged
  where ss0paged = set' (registers . ps) 0b1110011100000000 initialSystemState
ssMovedPaged = set memory (testMovingPages) ss0paged
  where ss0paged = set' (registers . ps) 0b1110011100000000 initialSystemState
ssDevPaged = set memory (testDevPages) ss0paged
  where ss0paged = set' (registers . ps) 0b1110011100000000 initialSystemState

--We'll set the first page each for ASIDs 0 and 1
testPages :: ByteString
--ASID 0 page 0 will point to frame 4 (0x1000)
testPages = mem & (ix 0x480) .~ 0b00000001 & (ix 0x481) .~ 0b00010000 &
--ASID 1 page 0 will point to frame 5 (0x1400)
  (ix 0x500) .~ 0b00000001 & (ix 0x501) .~ 0b00010100
  --frame 4 will have [6,1]
  where mem = initialSystemState ^. memory & (ix 0x1000) .~ 6 & (ix 0x1001) .~ 1 &
  --frame 5 will have [12.2]
              (ix 0x1400) .~ 12 & (ix 0x1401) .~ 2

testDevPages :: ByteString
--We'll set ASID 0 page 0 to point to frame 0 of device 1
testDevPages = mem  & (ix 0x480) .~ 0b00010001 & (ix 0x481) .~ 0b00000000
  where mem = initialSystemState ^. memory

--ASID 0 page 0 will point to frame 4 (0x1000)
--However, we move the root over to 0x600 (no reason for the number, just
-- "somewhere else")
testMovingPages = mem & (ix 0x600) .~ 0b00000001 & (ix 0x601) .~ 0b00010000
  --frame 4 will have [6,1]
  where mem = initialSystemState ^. memory & (ix 0x1000) .~ 6 & (ix 0x1001) .~ 1

testInstr i s = run $ execStateT (execute i) s

instance Arbitrary GPRegister where
  arbitrary = oneof $ fmap return [R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15]
instance Arbitrary TRegister where
  arbitrary = oneof $ fmap return [T0,T1,T2,T3,T4,T5,T6,T7]
