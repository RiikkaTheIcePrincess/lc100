{-# LANGUAGE OverloadedStrings, BinaryLiterals, FlexibleInstances #-}
module LC100.Test.BitTestTests where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.ByteString
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Data.ByteString.Lazy as BSL (take, drop, pack)
import Data.Word
import Control.Monad.State
import Control.Lens hiding (pre)

import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions
import LC100.Memory
import LC100.Test.Utils

bitTestTests = do
  describe "QC bit-testing tests" $ do
    it "BTX" $ property testBTXQuick
    it "BTC" $ property testBTCQuick
    it "BTS" $ property testBTSQuick

testBTXQuick val idx reg = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 & (mapGP reg) .~ val }
  res <- testInstr (G2Instr BTX reg idx) ss2
  let expected = complementBit val (fromIntegral idx)
  assert $ res ^. registers . mapGP reg == if reg == R0 then 0 else expected

testBTCQuick val idx reg = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 & (mapGP reg) .~ val }
  res <- testInstr (G2Instr BTC reg idx) ss2
  let expected = clearBit val (fromIntegral idx)
  assert $ res ^. registers . mapGP reg == if reg == R0 then 0 else expected
  
testBTSQuick val idx reg = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 & (mapGP reg) .~ val }
  res <- testInstr (G2Instr BTS reg idx) ss2
  let expected = setBit val (fromIntegral idx)
  assert $ res ^. registers . mapGP reg == if reg == R0 then 0 else expected
