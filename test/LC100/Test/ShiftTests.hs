module LC100.Test.ShiftTests where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.ByteString

import Data.Binary
import Data.Bits
import Data.Word
import Data.Int
import Data.ByteString.Lazy as BSL
import Control.Lens hiding (pre)
import Control.Monad.State

import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions

ss0 = initialSystemState { _registers = rs0 {_r7 = 0x7030 } }
rs1 = rs0{_r1=1,_r2=2,_r3=3,_r4=4,_r5=5,_r6=6,_r7=7,_r8=7} 
ss1 = ss0 { _memory = pack [0,1,2,3,4,5,6,7,8,9,10],
            _registers=rs1}
ss1_16 = ss0 { _memory = pack [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0,8,0,9,0,10,0],
            _registers=rs1}
instance Arbitrary GPRegister where
  arbitrary = oneof $ fmap return [R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15]
testInstr i s = run $ execStateT (execute i) s

shfTests = do
  describe "Pre-formed SHF tests" $ do
    testROLPre
    testRORPre
  describe "Quickcheckified SHF tests" $ parallel $ do
    it "ASL" $
      property testASLQuick
    it "ASR" $
      property testASRQuick
    it "LSL" $
      property testLSLQuick
    it "LSR" $
      property testLSRQuick
    it "ROL" $
      property testROLQuick
    it "ROR" $
      property testRORQuick

testROLPre = describe "ROL" $ do
  it "works with: ROL 3, R7 (0x7030)" $
    execStateT (execute $ G2Instr ROL R7 3) ss0 `shouldReturn`
      (ss0 { _registers = rs0 {_r7 = 33155} })
      
testRORPre = describe "ROR" $ do
  it "works with: ROR 3, R7 (0x7030)" $
    execStateT (execute $ G2Instr ROR R7 3) ss0 `shouldReturn`
      (ss0 { _registers = rs0 {_r7 = 3590} })

testASLQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (G2Instr ASL dest dist) ss2
  let exp = shiftL (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  assert $ (if dest == R0 then 0 else exp) == act

testLSLQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (G2Instr LSL dest dist) ss2
  let exp = shiftL (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  assert $ (if dest == R0 then 0 else exp) == act

testROLQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (G2Instr ROL dest dist) ss2
  let exp = rotateL (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  assert $ (if dest == R0 then 0 else exp) == act

testASRQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (G2Instr ASR dest dist) ss2
  let exp = shiftR (fromIntegral (val :: Word16) :: Int16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  assert $ (if dest == R0 then 0 else exp) == fromIntegral act

testLSRQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (G2Instr LSR dest dist) ss2
  let exp = shiftR (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  assert $ (if dest == R0 then 0 else exp) == act

testRORQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (G2Instr ROR dest dist) ss2
  let exp = rotateR (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  assert $ (if dest == R0 then 0 else exp) == act
