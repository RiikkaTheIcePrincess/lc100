{-# LANGUAGE OverloadedStrings, BinaryLiterals, FlexibleInstances #-}
module LC100.Test.JumpTests where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.ByteString
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Data.ByteString.Lazy as BSL (take, drop, pack)
import Data.Word
import Control.Monad.State
import Control.Lens hiding (pre)

import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions
import LC100.Memory
import LC100.Test.Utils

jumpTests = do
  describe "QC jump tests" $ do
    it "JMP"  $ property testJMPQuick
    it "JAL"  $ property testJALQuick

testJMPQuick tgt reg0 reg1 oldPC = monadicIO $ do
  --We won't overwrite R0 and test before it can be reset
  pre (reg0 /= R0)
  let rt = rs0 & pc .~ oldPC
  let ss2 = ss0 { _registers=rt & (mapGP reg0) .~ tgt }
  res <- testInstr (JMPInstr JMP reg0 reg1) ss2
  assert $ res ^. registers . pc == tgt

testJALQuick tgt reg0 reg1 oldPC = monadicIO $ do
  --We won't overwrite R0 and test before it can be reset
  pre (reg0 /= R0)
  --Whoopsy.
  pre (reg1 /= R0)
  let rt = rs0 & pc .~ oldPC
  let ss2 = ss0 { _registers=rt & (mapGP reg0) .~ tgt }
  res <- testInstr (JMPInstr JAL reg0 reg1) ss2
  assert $ res ^. registers . pc == tgt
  assert $ res ^. registers . mapGP reg1 == (oldPC+2)
