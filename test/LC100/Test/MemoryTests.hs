{-# LANGUAGE OverloadedStrings, BinaryLiterals #-}
module LC100.Test.MemoryTests where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.ByteString
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Data.ByteString.Lazy as BSL (take, drop, pack)
import Data.Word
import Control.Monad.State
import Control.Lens hiding (pre)

import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions
import LC100.Memory
import LC100.Test.Utils

memoryTests = do
  describe "Pre-formed memory tests" $ do
    it "sequential read two bytes ASID 0 page 0 (frame 4)" $ property testPaging0
    it "read one word ASID 0 page 0 (frame 4)" $ property testPaging1
    it "sequential read two bytes ASID 1 page 1 (frame 5)" $ property testPaging2
    it "read one word ASID 1 page 1 (frame 5)" $ property testPaging3
    it "sequential read 2B ASID 0 pg 0 (frame 4), moved root page table" $ property testMovingPaging0
    it "read one word ASID 0 pg 0 (frame 4), moved root page table" $ property testMovingPaging1    

testPaging0 = monadicIO $ do
  --pre-set values defined in Utils.hs
  let exp0 = 6
  let exp1 = 1
  let ss2 = ssPaged {_registers = ssPaged ^. registers & r1 .~ 1}
  res0 <- testInstr (G1Instr LOD R8 R0 True) ss2 --Read from address 0 to R8
  res1 <- testInstr (G1Instr LOD R8 R1 True) ss2 --Read from address 1 to R8
  assert $ res0 ^. registers . r8 == exp0
  assert $ res1 ^. registers . r8 == exp1

testPaging1 = monadicIO $ do
  --pre-set values defined in Utils.hs
  let exp = 262
  let ss2 = ssPaged {_registers = ssPaged ^. registers & r1 .~ 1}
  res0 <- testInstr (G1Instr LOD R8 R0 False) ss2 --Read from address 0:1 to R8
  assert $ res0 ^. registers . r8 == exp
  
testPaging2 = monadicIO $ do
  --pre-set values defined in Utils.hs
  let exp0 = 12
  let exp1 = 2
  let ss2 = ssPaged {_registers = ssPaged ^. registers & r1 .~ 1 & atr .~ 0x2090}
  res0 <- testInstr (G1Instr LOD R8 R0 True) ss2 --Read from address 0 to R8
  res1 <- testInstr (G1Instr LOD R8 R1 True) ss2 --Read from address 1 to R8
  assert $ res0 ^. registers . r8 == exp0
  assert $ res1 ^. registers . r8 == exp1
  
testPaging3 = monadicIO $ do
  --pre-set values defined in Utils.hs
  let exp = 0x020C
  let ss2 = ssPaged {_registers = ssPaged ^. registers & r1 .~ 1 & atr .~ 0x2090}
  res0 <- testInstr (G1Instr LOD R8 R0 False) ss2 --Read from address 0:1 to R8
  assert $ res0 ^. registers . r8 == exp

testMovingPaging0 = monadicIO $ do
  --pre-set values defined in Utils.hs
  let exp0 = 6
  let exp1 = 1
  let ss2 = ssMovedPaged {_registers = ssPaged ^. registers & r1 .~ 1 & atr .~ 192}
  res0 <- testInstr (G1Instr LOD R8 R0 True) ss2 --Read from address 0 to R8
  res1 <- testInstr (G1Instr LOD R8 R1 True) ss2 --Read from address 1 to R8
  assert $ res0 ^. registers . r8 == exp0
  assert $ res1 ^. registers . r8 == exp1

testMovingPaging1 = monadicIO $ do
  --pre-set values defined in Utils.hs
  let exp = 262
  let ss2 = ssMovedPaged {_registers = ssPaged ^. registers & r1 .~ 1 & atr .~ 192}
  res0 <- testInstr (G1Instr LOD R8 R0 False) ss2 --Read from address 0:1 to R8
  assert $ res0 ^. registers . r8 == exp
  
