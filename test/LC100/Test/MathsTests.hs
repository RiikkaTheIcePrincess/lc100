{-# LANGUAGE OverloadedStrings, BinaryLiterals, FlexibleInstances, ScopedTypeVariables #-}
module LC100.Test.MathsTests where
import Test.Syd
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
--import Test.QuickCheck.Instances.Vector
import Test.QuickCheck.Instances.ByteString

import Prelude hiding (length)
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Data.ByteString.Lazy as BSL (take, drop, pack, length,iterate)
import Data.Word
import Data.Maybe (fromMaybe)
import Control.Lens hiding (pre)
import Control.Monad.State
import qualified Control.Monad as CM

--import LC100
import LC100.Misc
import LC100.SystemState
import LC100.BitPatterns
import LC100.Instructions
import LC100.Memory
import LC100.Test.Utils

aluTests = do
  describe "Pre-formed maths tests" $ do
    compositePre
    testNEGPre
    testADDPre
    testSUBPre
    testMLSPre
  describe "QuickCheckified maths tests" $ do
    testMOVQuick
    testNEGQuick
    testADDQuick
    testSUBQuick
    testMULQuick
    testDIVQuick
    testMLSQuick
    testDVSQuick
    testMODQuick
    testMDSQuick
    testANDQuick
    testORQuick
    testXORQuick
    testNOTQuick
    testLODQuick
    testSTOQuick

--Temporary, should probably do this at a lower level
--before we read out operands, so we can just say to add registers
--rather than having to pluck out values from the state
compositePre = describe "Composite tests" $ do
  let inst0 = G1Instr ADD R7 R7 False --  7+7
  state0 <- runIO $ execStateT (execute inst0) ss1
  let inst1 = G1Instr MUL R7 R2 False --  *2
  state1 <- runIO $ execStateT (execute inst1) state0
  let inst2 = G1Instr DIV R7 R4 False --  /4
  state2 <- runIO $ execStateT (execute inst2) state1
  let inst3 = G1Instr SUB R7 R1 False --  -1
  state3 <- runIO $ execStateT (execute inst3) state2
  it "Calculates ((((7+7)*2)/4)-1) == 6" $
    state3 `shouldSatisfy` (\s -> _r7 (s ^. registers) == 6)

testNEGPre = describe "NEG" $ do
  it "works with: NEG, 0, 8-bit, R0" $
    execStateT (execute $ G1Instr NEG R0 R0 True) ss1 `shouldReturn`
      (ss1 { _registers = rs1 { _r0 = 0 } })
  it "works with: NEG, 1, 8-bit, R1" $
    execStateT (execute $ G1Instr NEG R1 R0 True) ss1 `shouldReturn`
      (ss1 { _registers = rs1 { _r1 = 0xFF } })
  it "works with: NEG, 0, 16-bit, R9" $
    execStateT (execute $ G1Instr NEG R9 R0 False) ss1 `shouldReturn`
      (ss1 { _registers = rs1 { _r9 = 0 } })
  it "works with: NEG, 1, 16-bit, R1" $
    execStateT (execute $ G1Instr NEG R1 R0 False) ss1 `shouldReturn`
      (ss1 { _registers = rs1 { _r1 = 0xFFFF } })

testADDPre = describe "ADD" $ do
  let rs2 = rs0 {_r3 = 39, _r6 = 89}
  let ss2 = ss0 { _registers=rs2 }
  it "works with: ADD, 39, 89, 8-bit, R3+R6" $
    execStateT (execute $ G1Instr ADD R3 R6 True) ss2 `shouldReturn`
      (ss2 { _registers = rs2 {_r3 = 128, _r6 = 89} })
  let rs3 = rs0 {_r6 = 53, _r8 = 75}
  let ss3 = ss0 { _registers = rs3 }
  it "works with: ADD, 53, 75, 8-bit, R6+R8" $
    execStateT (execute $ G1Instr ADD R6 R8 True) ss3 `shouldReturn`
      (ss3 { _registers = rs3 {_r6 = 128} })

testSUBPre = describe "SUB" $ do
  let rs2 = rs0 {_r9 = 0, _r1 = 1, _r6 = 6}
  let ss2 = ss0 { _registers=rs2 }
  it "works with: SUB, 0, 1, 8-bit, R9-R1" $
    execStateT (execute $ G1Instr SUB R9 R1 True) ss2 `shouldReturn`
      (ss2 { _registers = rs2 {_r9 = 255} })
  it "works with: SUB, 0, 1, 16-bit, R9-R1" $
    execStateT (execute $ G1Instr SUB R9 R1 False) ss2 `shouldReturn`
      (ss2 { _registers = rs2 {_r9 = 65535} })
  it "works with: SUB, 6, 0, 16-bit, R6-R0" $
    execStateT (execute $ G1Instr SUB R6 R0 False) ss2 `shouldReturn`
      (ss2 { _registers = rs2 {_r6 = 6} })

testMLSPre = describe "MLS" $ do
  let rs2 = rs0 {_r6 = 0xFFFF, _r7 = 7, _r9 = 0xFF}
  let ss2 = ss0 { _registers=rs2 }
  it "works with: MLS, -1, 7, 8-bit, R9*R7" $
    execStateT (execute $ G1Instr MLS R9 R7 True) ss2 `shouldReturn`
      (ss2 { _registers = rs2 {_r8 = 0xFF, _r9 = fromIntegral (249 :: Word8)} })
  it "works with: MLS, -1, 7, 16-bit, R7*R6" $
    execStateT (execute $ G1Instr MLS R7 R6 False) ss2 `shouldReturn`
      (ss2 { _registers = rs2 {_r8 = 0xFFFF, _r7 = (65529)} }) 

testMOVQuick = describe "MOV" $ do
  it "moves 8-bit stuff correctly" $
    property movQuick8
  it "moves 16-bit stuff correctly" $
    property movQuick
    
testNEGQuick = describe "NEG" $ do
  it "negates 8-bit stuff correctly" $
    property negQuick8
  it "negates 16-bit stuff correctly" $
    property negQuick

testADDQuick = describe "ADD" $ do
  it "adds 8-bit stuff correctly" $
    property addQuick8
  it "adds 16-bit stuff correctly" $
    property addQuick

testSUBQuick = describe "SUB" $ do
  it "subtracts 8-bit stuff correctly" $
    property subQuick8
  it "subtracts 16-bit stuff correctly" $
    property subQuick

testMULQuick = describe "MUL" $ do
  it "does the 8-bit multiply thing" $
    property mulQuick8
  it "does the 16-bit multiply thing" $
    property mulQuick

testDIVQuick = describe "DIV" $ do
  it "dances the 8-bit division shuffle" $
    property divQuick8
  it "dances the 16-bit division shuffle" $
    property divQuick

testMLSQuick = describe "MLS" $ do
  it "signedly multiplies 8-bit-ish stuff" $
    property mliQuick8
  it "signedly multiplies 16-bitly" $
    property mliQuick

testDVSQuick = describe "DVS" $ do
  it "signedly divides 8-bitters" $
    property dviQuick8
  it "signedly divides 16-bitlets" $
    property dviQuick

testMODQuick = describe "MOD" $ do
  it "moduluses 8-bit whatevers" $
    property modQuick8
  it "moduluses 16-bit things" $
    property modQuick

testMDSQuick = describe "MDS" $ do
  it "signedly moduluses 8-bit things" $
    property mdiQuick8
  it "signedly moduluses 16-bit whatevers" $
    property mdiQuick

testANDQuick = describe "AND" $ do
  it "ANDs 8-bit things" $
    property andQuick8
  it "ANDs 16-bit whatevers" $
    property andQuick

testORQuick = describe "OR" $ do
  it "ORs 8-bit things" $
    property orQuick8
  it "ORs 16-bit whatevers" $
    property orQuick

testXORQuick = describe "XOR" $ do
  it "XORs 8-bit things" $
    property xorQuick8
  it "XORs 16-bit whatevers" $
    property xorQuick

testNOTQuick = describe "NOT" $ do
  it "NOTs 8-bit things" $
    property notQuick8
  it "NOTs 16-bit whatevers" $
    property notQuick --[Joke] Very slow D:

testLODQuick = describe "LOD" $ do
  it "LODs 8-bit things" $
    property lodQuick8
  it "LODs 16-bit whatevers" $
    property lodQuick

testSTOQuick = describe "STO" $ do
  it "STOs 8-bit things" $
    property stoQuick8
  it "STOs 16-bit whatevers" $
    property stoQuick

movQuick8 op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr MOV R11 R12 True) ss2
  let newVal = op1 .&. 0xFF
  assert $ res ^. registers . r11 == newVal
  
movQuick op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr MOV R11 R12 False) ss2
  let newVal = op1
  assert $ res ^. registers . r11 == newVal
  
negQuick8 op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr NEG R11 R12 True) ss2
  let newVal = (negate op0) .&. 0xFF
  --Maybe find another way to test this than relying on
  --  the same conversion code?
  assert $ res ^. registers . r11 == newVal
  
negQuick op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr NEG R11 R12 False) ss2
  let newVal = negate op0
  assert $ res ^. registers . r11 == newVal
  
addQuick8 op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr ADD R11 R12 True) ss2
  --Seems there aren't many ways to go about this.
  --Ugh. Just... test everything's set? I don't know what else to do here.
  --let sumU = w8ToW16 (w16ToW8 op0 + w16ToW8 op1)
  let sumU = 0xFF .&. (op0 + op1)
  --Check register
  assert $ res ^. registers . r11 == sumU
  
addQuick op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr ADD R11 R12 False) ss2
  let sumU = op0 + op1
  --Check register
  assert $ res ^. registers . r11 == sumU

subQuick8 op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr SUB R11 R12 True) ss2
  --let sumU = w8ToW16 (w16ToW8 op0 - w16ToW8 op1) --Slow!
  --let sumU = 0xFF .&. ((0xFF .&. op0) - (0xFF .&. op1)) --Better but still slow
  let sumU = 0xFF .&. (op0 - op1)
  --Check register
  assert $ res ^. registers . r11 == sumU

subQuick op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr SUB R11 R12 False) ss2
  let sumU = op0 - op1
  --Check register
  assert $ res ^. registers . r11 == sumU

mulQuick8 op0 op1 reg0 reg1 = monadicIO $ do
  pre (reg0 /= reg1)
  let rs2 = rs0 & mapGP reg0 .~ op0
  let ss2 = ss0 { _registers= rs2 & mapGP reg1 .~ op1}
  res <- testInstr (G1Instr MUL reg0 reg1 True) ss2
  let prod = (op0 .&. 0x00FF) * (op1 .&. 0x00FF)
  assert $ (res ^. registers . mapGP R8) ==
    --R8/t0 is overwritten if it's dest
    if reg0 == R8 then prod .&. 0x00FF
    else prod .&. 0xFF00
  assert $ res ^. registers . mapGP reg0 == (if reg0 == R0 then 0 else prod .&. 0x00FF)

mulQuick op0 op1 reg0 reg1 = monadicIO $ do
  pre (reg0 /= reg1)
  let rs2 = rs0 & mapGP reg0 .~ op0
  let ss2 = ss0 { _registers= rs2 & mapGP reg1 .~ op1}
  res <- testInstr (G1Instr MUL reg0 reg1 False) ss2
  let prod = (w16ToW32 op0) * (w16ToW32 op1) :: Word32
  let hw     = prod .&. 0xFFFF0000
  let hiWord = runGet getWord16be $ BSL.take 2 $ encode hw
  let loWord = fromIntegral $ prod .&. 0x0000FFFF :: Word16
  assert $ res ^. registers . mapGP R8 ==
    if reg0 == R8 then loWord
    else hiWord
  assert $ res ^. registers . mapGP reg0 == (if reg0 == R0 then 0 else loWord)

--TODO check DivZeroFault when implemented
divQuick8 op0 op1
  | op1 .&. 0x00FF == 0 = discard --Discard this test case
  | otherwise = monadicIO $ do
      let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
      res <- testInstr (G1Instr DIV R11 R12 True) ss2
      let quot = (op0 .&. 0x00FF) `div` (op1 .&. 0x00FF)
      assert $ res ^. registers . r11 == quot

--TODO check DivZeroFault here too
divQuick op0 op1
  | op1 == 0 = discard
  | otherwise = monadicIO $ do
      let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
      res <- testInstr (G1Instr DIV R11 R12 False) ss2
      let quot = op0 `div` op1
      assert $ res ^. registers . r11 == quot

mliQuick8 op0 op1 reg0 reg1 = monadicIO $ do
  --Really sick of just testing the data is set
  --rather than testing the maths/logics
  pre (reg0 /= reg1)
  let rs2 = rs0 & mapGP reg0 .~ op0
  let ss2 = ss0 { _registers= rs2 & mapGP reg1 .~ op1}
  res <- testInstr (G1Instr MLS reg0 reg1 True) ss2
  let lil1 = fromIntegral (w8ToInt8 $ w16ToW8 op0) :: Int16
  let lil2 = fromIntegral (w8ToInt8 $ w16ToW8 op1) :: Int16
  let prod = lil1 * lil2
  let loByte = runGet getInt8 $ BSL.drop 1 $ encode prod
  let hiByte = runGet getInt8 $ BSL.take 1 $ encode prod
  let prodU = runGet getWord16be $ encode prod
  assert $ res ^. registers . mapGP R8 ==
    --t0/r8 is overwritten if it's dest
    if reg0 == R8 then prodU .&. 0x00FF
      else shiftR prodU 8
  assert $ res ^. registers . mapGP reg0 == (if reg0 == R0 then 0 else prodU .&. 0x00FF)

mliQuick op0 op1 reg0 reg1 = monadicIO $ do
  pre (reg0 /= reg1)
  let rs2 = rs0 & mapGP reg0 .~ op0
  let ss2 = ss0 { _registers= rs2 & mapGP reg1 .~ op1}
  res <- testInstr (G1Instr MLS reg0 reg1 False) ss2
  let lil1 = w16ToInt32 op0 :: Int32
  let lil2 = w16ToInt32 op1 :: Int32
  let prod = lil1 * lil2
  let loWord = runGet getInt16be $ BSL.drop 2 $ encode prod
  let hiWord = runGet getInt16be $ BSL.take 2 $ encode prod
  let prodU = runGet getWord32be $ encode prod
  assert $ res ^. registers . mapGP R8 ==
    --t0/r8 is overwritten if it's dest
    if reg0 == R8 then fromIntegral (prodU .&. 0x0000FFFF)
      else fromIntegral (shiftR prodU 16)
  assert $ res ^. registers . mapGP reg0 == (if reg0 == R0 then 0 else fromIntegral (prodU .&. 0x0000FFFF))

--TODO check DivZeroFault when implemented
--Leaving out ALUAddressing randomisation (op2) for simplicity
dviQuick8 op0 op1
  | op1 .&. 0x00FF == 0 = discard
  | otherwise = monadicIO $ do
      let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
      res <- testInstr (G1Instr DVS R11 R12 True) ss2
      let lil1 = w8ToInt8 $ w16ToW8 op0 :: Int8
      let lil2 = w8ToInt8 $ w16ToW8 op1 :: Int8
      let result = lil1 `div` lil2
      let result' = int8ToW16 result
      assert $ res ^. registers . r11 == result' .&. 0x00FF

--TODO check DivZeroFault when implemented
dviQuick op0 op1
  | op1 == 0 = discard
  | otherwise = monadicIO $ do
      let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
      res <- testInstr (G1Instr DVS R11 R12 False) ss2
      let lil1 = w16ToInt16 op0 :: Int16
      let lil2 = w16ToInt16 op1 :: Int16
      let result = lil1 `div` lil2
      let result' = int16ToW16 result
      assert $ res ^. registers . r11 == result'

--TODO check DivZeroFault when implemented
modQuick8 op0 op1
  | op1 .&. 0x00FF == 0 = discard --Discard this test case
  | otherwise = monadicIO $ do
      let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
      res <- testInstr (G1Instr MOD R11 R12 True) ss2
      let lilMod = (op0 .&. 0x00FF) `mod` (op1 .&. 0x00FF)
      assert $ res ^. registers . r11 == lilMod

--TODO check DivZeroFault here too
modQuick op0 op1
  | op1 == 0 = discard
  | otherwise = monadicIO $ do
      let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
      res <- testInstr (G1Instr MOD R11 R12 False) ss2
      let bigMod = op0 `mod` op1
      assert $ res ^. registers . r11 == bigMod

--TODO check DivZeroFault when implemented
mdiQuick8 op0 op1
  | op1 .&. 0x00FF == 0 = discard
  | otherwise = monadicIO $ do
      let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
      res <- testInstr (G1Instr MDS R11 R12 True) ss2
      let lil1 = w8ToInt8 $ w16ToW8 op0 :: Int8
      let lil2 = w8ToInt8 $ w16ToW8 op1 :: Int8
      let result = lil1 `mod` lil2
      let result' = int8ToW16 result
      assert $ res ^. registers . r11 == result' .&. 0x00FF

--TODO check DivZeroFault when implemented
mdiQuick op0 op1
  | op1 == 0 = discard
  | otherwise = monadicIO $ do
      let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
      res <- testInstr (G1Instr MDS R11 R12 False) ss2
      let lil1 = w16ToInt16 op0 :: Int16
      let lil2 = w16ToInt16 op1 :: Int16
      let result = lil1 `mod` lil2
      let result' = int16ToW16 result
      assert $ res ^. registers . r11 == result'

andQuick8 op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr AND R11 R12 True) ss2
  let lilResult = (op0 .&. 0x00FF) .&. (op1 .&. 0x00FF)
  assert $ res ^. registers . r11 == lilResult

andQuick op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr AND R11 R12 False) ss2
  let result = op0 .&. op1
  assert $ res ^. registers . r11 == result

orQuick8 op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr OR R11 R12 True) ss2
  let lilResult = (op0 .&. 0x00FF) .|. (op1 .&. 0x00FF)
  assert $ res ^. registers . r11 == lilResult

orQuick op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr OR R11 R12 False) ss2
  let result = op0 .|. op1
  assert $ res ^. registers . r11 == result

xorQuick8 op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr XOR R11 R12 True) ss2
  let lilResult = (op0 .&. 0x00FF) `xor` (op1 .&. 0x00FF)
  assert $ res ^. registers . r11 == lilResult

xorQuick op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr XOR R11 R12 False) ss2
  let result = op0 `xor` op1
  assert $ res ^. registers . r11 == result

notQuick8 op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr NOT R11 R12 True) ss2
  let lilResult = complement (op0 .&. 0x00FF) .&. 0x00FF
  assert $ res ^. registers . r11 == lilResult

notQuick op0 op1 = monadicIO $ do
  let ss2 = ss0 { _registers=rs0 {_r11 = op0, _r12 = op1} }
  res <- testInstr (G1Instr NOT R11 R12 False) ss2
  let result = complement op0
  assert $ res ^. registers . r11 == result

--Only testing direct physical memory access at the moment
--TODO test virtual memory
--TODO handle SILOs and mapped regions
--addr to mem, reg is register destination
lodQuick8 op0 (addrpre :: Word16) = monadicIO $ do
  let ram = BSL.take (2^16) $ BSL.iterate (+1) 6
  --We're not testing ROM/SILO loads here
  let addr = if addrpre >= 0x400 then addrpre else addrpre+0x400 :: Word16
  let ss2 = ss1 {_memory = ram, _registers=rs0 {_r11 = op0, _r12 = addr}}
  res <- testInstr (G1Instr LOD R11 R12 True) ss2
  let addr' = fromIntegral $ addr
  let result = ss2 ^. memory . singular (ix addr')
  assert $ res ^. registers . r11 == w8ToW16 result
  --assert $ res ^. registers . r11 == w8ToW16 result

lodQuick op0 addrReg (addrpre :: Word16) = monadicIO $ do
  let ram = BSL.take (2^16) $ BSL.iterate (+1) 6
  pre (0x400 < BSL.length ram)
  let addr = if addrpre >= 0x400 then addrpre else addrpre+0x400 :: Word16
  let rs = rs0 {_r11 = op0} --Random initial value in the register
  let ss2 = ss1 {_memory = ram, _registers=rs & mapGP addrReg .~ addr}
  res <- testInstr (G1Instr LOD R11 addrReg False) ss2
  let addr' = fromIntegral addr
  let result = runGet getWord16le $
               pack [ss2 ^. memory . singular (ix addr')
                    ,ss2 ^. memory . singular (ix (addr'+1))]
  assert $ res ^. registers . r11 == result

stoQuick8 (addrpre :: Word16) addrReg srcVal = monadicIO $ do
  pre (addrReg /= R3)
  let addr = if addrpre >= 0x400 then addrpre else addrpre+0x400 :: Word16
  let rs2 = rs1 & r3 .~ (srcVal .&. 0xFF)
  let ss2 = ss0 { _registers = (rs2 & mapGP addrReg .~ addr) }
  res <- testInstr (G1Instr STO R3 addrReg True) ss2
  let addr' = fromIntegral addr
  let result = res ^. memory . singular (ix addr')
  let regVal = res ^. registers . r3
  assert $ (srcVal .&. 0xFF) == w8ToW16 result

stoQuick (addrpre :: Word16) addrReg srcVal = monadicIO $ do
  pre (addrReg /= R3)
  let addr = if addrpre >= 0x400 then addrpre else addrpre+0x400 :: Word16
  let rs2 = rs1 & r3 .~ srcVal
  let ss2 = ss0 { _registers = (rs2 & mapGP addrReg .~ addr) }
  res <- testInstr (G1Instr STO R3 addrReg False) ss2
  let addr' = fromIntegral addr
  let result = runGet getWord16le $
               pack [res ^. memory . singular (ix addr')
                    ,res ^. memory . singular (ix (addr'+1))]
  let regVal = res ^. registers . r3
  assert $ srcVal == result
  
