This document is meant to serve as a specification for the LC100 Virtual System
Description file format. Blah blah blah let's get started specifying some crap.

| Address       | Purpose                                                         |
| ------------- | --------------------------------------------------------------- |
| 0x0-0x1       | 16b word specifying available RAM                               |
| 0x2-0x1C3     | 15 30B NUL-terminated UTF-8 strings specifying devices to load  |
| 0x1C4-0x3BF   | 508B of Idunno- Imean, reserved for future use                  |
| 0x3C0-0x3FF   | 64B init ROM                                                    |

Everything's little-endian. The strings are UTF-8, so no endianness concerns there.
RAM is specified in bytes; 65535B will be treated as 65536B available. 
All contents are 0-padded to fit the specified field widths.
