LC100 Lili-Core (0x10-bit) (Model 0x0) Processor Specification  
Spec version 0.9 (it's starting to feel pretty done)

# Table of Contents
1. [Introduction](#introduction)
   1. [Public Domain Dedication](#public-domain-dedication)
   2. [Conventions](#conventions)
   3. [Origins/YCPU Credit](#origins-and-ycpu-credit)
2. [Hardware Information](#hardware-information)
   1. [Memory Model](#memory-model)
   2. [Instruction Bit Width](#instruction-bit-width)
   3. [Memory Alignment](#memory-alignment)
   5. [Registers](#registers)
      1. [Processor Control Registers](#processor-control-registers)
   6. [Supervisor and User Modes](#supervisor-and-user-modes)
   7. [Hardware Device Interface](#hardware-devices)
   8. [Interrupts](#interrupts)
   9. [Memory Management](#memory-management)
   10. [Boot Sequence](#boot-sequence)
   11. [Real Time Clock](#real-time-clock)
   12. [Dropping Privilege](#dropping-privilege)
3. [Instruction Set](#instruction-set)
   1. [Bit Patterns](#bit-patterns)
   2. [Instructions](#instructions)
      1. [Maths](#maths-instructions)
      2. [Increment](#increment-instructions)
      3. [Bit-shifting](#bit-shifting-instructions)
      4. [Bit-testing](#bit-testing-instructions)
      5. [Branch](#branch-instructions)
      6. [Jump](#jump-instructions)
      7. [Processor and Utility](#processor-and-utility-instructions)

# Introduction

The LC100 is a (virtual... for now? **spooky sounds/music**) little-endian 16-bit processor intended to be
a simple to use and implement playful little processor and perhaps basis
for a family of processors. Its general characteristics include:

- Sixteen general-purpose 16-bit registers
- Simplistic(ish) RISCy load-store architecture
- Two privilege levels
- 16-bit memory address space with paged virtual address spaces
- Real time clock with programmable interrupt timer
- Memory-mapped hardware access for up to fifteen devices

This document is arranged in two sections: firstly, a description
of the hardware that forms the basis for the system. Secondly, the
instruction set is described in chapters according to instruction type.

## Required Hardware

The basic hardware required for the system to operate include only the
processor itself, a read-only memory (ROM) device, and a
hardware bus connecting these with any random-access memory (RAM) or 
other devices (devices for input/output, networking, storage, processing,
etc. as needed). Naturally its usefulness is minimal without RAM and a means
of output but the processor can operate with only ROM available.
Note that the processor does not inherently track available memory and
devices; these may be queried from the memory map.

For full operation (as a microcomputer) at least 4KiB is expected.

## Conventions

- Constant values will typically be stated in decimal or with prefixes ("0x"
for hexadecimal, "0b" for binary) 
- Bit values will be stated numerically (1/0) or as "high" or "low."[^bitval]
- Bit layouts will be stated like so:  

      FEDC BA98 7654 3210  
      aaaa bbcc defg hh..
  
  Bits are numbered from least to most significant and stated in
  hexadecimal above symbols denoting their meaning. For example, it may
  be stated that "**a** is a four-bit field denoting the destination
  register" meaning that the four most significant bits (indices F,E,D,
  and C) shall be read as a four-bit value whose most-significant bit is
  at the left (bit F). Sometimes a field may be referred to using the
  register name and its letter identifier; for example, PS.A would be
  the A field in the PS register. A **.** in such a specification means that
  that bit position is not used in that context. Particularly, opcodes are
  specified with "." used to denote bit positions not used *by the opcode*
  though those may be used for instruction data.

[^bitval]: Unless I oops up and leave in some bit of YCPU spec with "set"/"clear" in it :P

## Public Domain Dedication

Sharing the spirit of YCPU, this document is dedicated to the public
domain under the terms of the Creative Commons Zero (CC0) waiver.
All rights have been waived with regard to copyright law, including
all related and neighboring rights, to the extent allowed by law.

You can copy, modify, and distribute this document, and use it to
create derivative works, even for commercial purposes, all without
asking permission.

More information about CC0 waiver is available online at: 
http://creativecommons.org/about/cc0

## Origins and YCPU Credit

This document is based significantly on the YCPU specification created
by Zane Wagner
(https://github.com/ZaneDubya/YCPU/blob/master/Documentation/ycpu.txt)
and my experience with it. Inspiration is also taken from various
ISAs including historical ISAs and RISC-V. It is hoped
that this will grow into a fun and unique thing.

# Hardware Information

The LC family of processors is designed for simplicity of use and
implementation with a reasonable range of applicability. The LC10x is
intended to serve as the family's 16-bit member, a cute little thing 
with just the right number of bits. You heard me, 16-bit is best :P

## Memory Model

The LC100 provides a flat 16-bit (optionally virtual) memory space with
paged memory and virtual address spaces optionally enabled by the memory
management unit.

### Paging Model

LC100 (somewhat insistently) offers a paged memory system using 1KiB pages that
supports both virtual address spaces and mapping addon device memory into said
spaces. Paging with virtual address spaces uses a two-tier system. The first
tier identifies a process (or other address space type), while the
second tier maps virtual addresses to physical pages. 

## Instruction Bit Width

Instruction width is fixed at 16 bits wide. Instructions using the 
ALU bit pattern may be specified to operate in "eight-bit mode" in which
case they behave as follows:

  - Operations between registers clear the upper eight bits of the 
  destination register and ignore the upper eight bits of source register(s)
  - LOD and STO access only a single byte with no regard for alignment; LOD
  will clear the upper byte of the destination register and STO will ignore the
  upper byte of the source register.

No other instructions support eight-bit mode.

## Memory alignment

Memory is aligned on (16-bit) word boundaries. Memory access takes one
cycle for 8-bit and aligned 16-bit accesses and two cycles for unaligned
(to an odd-numbered address) 16-bit accesses.

## Registers

Sixteen general purpose 16-bit registers are available, named
r0 through r15, each allowing a range of immediate values on
\[0,65535] when interpreted as unsigned or \[-32768,32767] as
signed values. Multiplication expands to 32 bits and
may target up to two registers for storage of the low and 
high 16-bit words. These are used for all computations and
are the sources for or destinations of load and store instructions.
These registers also bear the following names and purposes for use with
the LC100 standard calling convention:

| Register | Alternate Name       | Purpose                  |
| -------- | -------------------- | ------------------------ |
| r0       | zero                 | Zero register            |
| r1       | ra                   | Return address           |
| r2       | bp                   | Base/frame pointer       |
| r3       | sp                   | Stack pointer            |
| r4-r7    | a0-a3                | Function arguments/return values |
| r8-r15   | t0-t7                | Temporary/any purpose    |

Also provided are four control registers that determine program flow
during execution. See [Processor Control Registers](#processor-control-registers)
for more information. The processor control registers are:

| Control Register | Meaning                            |
| ---------------- | ---------------------------------- |
| PC      | Program counter, pointer to the current instruction |
|         | within the active code bank                         |
| PS      | Processor status, various modes and information     |
| AT      | Address translation configuration                   |
| IRPC    | Program counter for returning from interrupts/exceptions |
| ISR     | Scratch register for use during interrupts          |

### Processor Control Registers

There exist five 16-bit processor control registers. Other than the SP
registers these are not directly accessible as standard register
operands.

  - PC: The program counter. This register holds the address of the next
  instruction to be executed. This register is incremented by two
  (two bytes, one 16-bit word) per executed instruction or set by
  branch and jump instructions.
  - PS: Processor status register. Flags state the current status (such as
  whether an interrupt is being handled) or are toggled to set modes such
  as supervisor mode.
    
        FEDC BA98 7654 3210  
        SPHI UZAA VVVV YYYY  
      
    - S - Supervisor mode enabled (set by processor, 0 is user mode)
    - P - Set high to enable the effect of paging during supervisor mode
    (disable to access all addresses directly as specified in the
    [layout](#memory-layout))
    - H - Hardware interrupts enabled (set to 1 to enable)
    - I - Currently handling interrupt (set by the processor)
    - U - Fault in user mode (set by processor)
    - Z - Sleeping flag, set by SLP to put the processor into a state in which
    it waits for a hardware interrupt before resuming execution. If the H flag
    is low, the system will halt but probably not catch fire.
    - A - Defines how many bits of the A register in AT shall be considered
    valid (MemFault if AT.A is >= 2^(PS.A) during memory access).
    - V - Interrupt value nibble (set by processor, contains ID of an active
    interrupt)
    - Y - Interrupt info nibble (set by processor, contains info on an
    active interrupt)
  - AT: Address Translation specification

        FEDC BA98 7654 3210  
        AAAP PPPP PPPP PPPP
      
    - A - Address space identifier (ASID, typically a process ID)
    - P - Base address of the root page table right shifted by 3
  - IRPC: Interrupt Return Program Counter (set by processor)
  - ISR: Interrupt Scratch Register, used during interrupt handling (set by
  processor)
  
## Supervisor and User Modes

Supervisor mode is enabled when the S bit in the PS register is set,
which is true on processor boot and whenever an interrupt is called.
When the S flag bit is low, the processor is in user mode.

In supervisor mode, running code:

  - Can execute privileged opcodes
  - Can set all bits in the process status register. Though of course
  it shouldn't, aside from the few specified as user-manipulable.
  - Can read and write to all processor control registers.

In user mode, running code:

  - Raises an 'Unprivileged Opcode' interrupt on executing privileged opcodes.
  - Cannot set any bits in the process status register.
  - Cannot write to any processor control registers, but can read from them.

## Hardware Devices

Software interacts with hardware using a memory-mapped IO (MMIO) scheme.
Basic interaction is facilitated via a static mapping but larger regions may be
mapped through the paging system. During startup, up to fifteen devices are
mapped into this page in 64-byte segments called SILOs
(Simple Input/List/Output segments[^SILO]) as follows:

  - First 16-bit word is filled with a "vendor" or other source ID
  - Second word is filled with a device ID
  - Third word indicates the amount of memory available for mapping
  - Fourth word indicates a waiting interrupt condition. The value specifies
  the condition or other value needed by a device driver, with semantics defined
  by the associated device interface protocol.
  - Remaining space follows a pattern specified by the device creator

Mapping into paged memory is performed by setting the D field in a page table
entry to the index of the device in this page, for example 2 for the third
device in the array of SILOs (64*2 bytes into the SILO region). The MMU will
then access device memory instead of system memory. An interrupt shall issue
upon access to memory beyond the limit specified by the device in this segment.
If memory beyond the actual boundary of memory but within the specified boundary
is accessed then behaviour is undefined though nasal demons may issue forth
only in the most amusing of circumstances.

[^SILO]: I just wanted a cute and easy name for these arrays :P

## Interrupts

An interrupt is a signal that indicates that an event requires immediate
attention. Interrupts can be triggered by a hardware device, by software using
the SWI opcode, or when the processor encounters a fault state. Each type of
interrupt is identified by an index from 0 to 15.

When the processor receives an interrupt, it gets the address of the interrupt
handler for the type of interrupt received from the Interrupt Vector Table
(IVT), an editable array of 16 16-bit addresses at the beginning of page 0x1.
Aside from the SWI opcode-initiated software interrupt and the
hardware-initiated interrupt, all interrupts are raised by the processor
itself. The interrupt IDs follow:

| ID       | Name          | Purpose                         |
| -------- | -------------------- | ------------------------ |
| 0x0      | Reset         | Raised when the processor is reset. Processor shall follow [boot procedure](#boot-sequence) |
| 0x1      | Clock         | Raised when the real time clock ticks |
| 0x2      | DivZeroFault  | Raised upon attempting to execute a division operation with a zero-value divisor |
| 0x3      | DoubleFault   | Raised when a fault occurs during the handling of another interrupt |
| 0x4      | UnprivFault   | Raised upon invocation of a privileged opcode while in user mode |
| 0x5      | UndefFault    | Raised upon invocation of an undefined opcode |
| 0x6      | HWInterrupt   | Interrupt raised by a hardware device |
| 0x7      | BusRefresh    | Change in hardware configuration (device added or removed) |
| 0x8      | SWInterrupt   | Interrupt raised deliberately by software |
| 0x9      | MemFault      | Fault in accessing memory (such as page not valid, address space ID invalid) |
| 0xA-0xF  | (Reserved)    | Reserved for future use  |

### Interrupt Subtypes

For some interrupts, PS.Y may indicate a subtype of interrupt.

  - MemFault interrupts may use PS.Y to indicate
    - 0 - Unknown or otherwise "figure it out yourself" generic error
    - 1 - Invalid ASID (ASID value higher than the limit specified in PS.A or 
    the maximum value of 6)
    - 2 - Attempt to access a page not marked valid (PTE's V bit is low)
  - Clock interrupts use PS.Y to indicate the index of the timer generating an
  interrupt.

### Hardware Interrupts

A device on the hardware bus may request the CPU's attention by setting the 
fourth field of its SILO to a nonzero value. The processor (or MMU, or other
chip on the bus) will detect any nonzero value and raise HWInterrupt if PS.I is
low (not currently handling an interrupt) and PS.H is high (hardware 
interrupts enabled).

### Software Interrupts

An interrupt may also be triggered by the SWI opcode. In this case, the
processor will use the 'software interrupt' interrupt vector and the code
at that interrupt must determine what function has been requested based on the
contents of the registers.

The PS.H status bit only applies to hardware interrupts, and thus software
interrupts will be acknowledged by the CPU if the H status bit is low. 

### Fault Interrupts

For interrupts caused by an error condition, the U status bit in PS will be high
if the processor was in user mode when the fault occured, or low if the
processor was in supervisor mode when the fault occured.

Interrupts designated as 'Faults' are:  

  - DivZeroFault  (0x2)
  - DoubleFault   (0x3)
  - UnprivFault   (0x4)
  - UndefFault    (0x5)
  - MemFault      (0x9)

### Double and Triple Faults

Should a fault occur during execution of an interrupt handler, that fault is 
converted into a DoubleFault, as interrupt handling has proven unreliable in
the current state. Note that PS.U will always be low in this situation as
interrupt handlers operate in supervisor mode.

Should a fault occur during DoubleFault handling, a "triple fault" has occurred
and the processor will reset.

### Interrupt Sequence

When an interrupt is raised, the processor halts the current executing process.
It then executes the following sequence of operations:

  1. Copy PS to ISR
  2. If the current interrupt is a fault, then  
      - If in user mode, set high PS.U
      - Else set low PS.U
  3. If the interrupt uses PS.Y then set that as appropriate
  4. Set PS.S and PS.I (supervisor and interrupt flags)
  5. Copy PC to IRPC
  6. Copy the index/ID of the interrupt to PS.V
  7. Set PC to (0x0400 + 2*(interrupt index))
  8. Resume execution

### Returning from an interrupt

When an interrupt handler ends it should call RTI, which will restore PS from
ISR (remember to keep a copy of that if using ISR for something else) and PC
from IPC. Before that, the interrupt handler should ensure that all registers
are in an appropriate state (either as they were in before the interrupt
or, if necessary, a new state needed to continue execution).

## Memory Management

Because the LC100 is a purely 16-bit system, the address space is limited to 
2^16 bytes. This is, however, supplemented by the ability to map memory from
added devices into the memory space of any process. Because this memory mapping
is implemented through the MMU's paging system, the MMU is not fully optional
though setting PS.A to zero will eliminate most of its functionality (and space
taken by page tables).

### Memory Layout

Happy birthday, here's some convenient memory layout stuff!

| Address       | Purpose                                               |
| ------------- | ----------------------------------------------------- |
| 0x0-0x39      | 64B for init ROM. Writes fail silently.               |
| 0x40-0x3FF    | 15*64B hardware SILOs                                 |
| 0x400-0x41F   | 16 16-bit pointers (the IVT)                          |
| 0x420-0x42F   | RTC Registers                                         |
| 0x430-0x47F   | System info/Reserved                                  |
| 0x480-0x7FF   | Default page table location (note that this varies
| |based on the value of PS.A; any space not needed for page tables is not
| |otherwise used or claimed by the processor)                          |
| 0x800-0xFFFF  | Available for use                                     |

Note that the first page (<0x0400) is exclusively ROM and MMIO and should not
be used for storage. Any writes to ROM shall be discarded silently.

### System information region

The area of memory from 0x430 to 0x43F shall contain system information
arranged as follows:

| Address       | Purpose                                                      |
| ------------- | ------------------------------------------------------------ |
| 0x430-0x431   | One word containing the number of bytes available in memory  |
| 0x432-0x47F   | Idunno, whatever else I forgot to put somewhere              |

65535B memory means 65536. Had to skip one somewhere so it'll be the last one.
...Don't worry about it >.<

### Paging

Address translation is performed as follows:

  - Get the address of the root page table by left shifting the P field
of the AT register by 4
  - Get the process's page table address by adding to the previous 
value the value of the A field (also in AT) multiplied by 2^7 (the interval
between page tables). A MemFault interrupt occurs when reading this A field if
it exceeds the allowed bit width specified in the PS register's A field. This
allows for reliably shrinking the space needed for page table entries by
reducing the number of available process IDs.
  - Obtain the PTE's address by applying the top six bits of the requested
  address multiplied by two (or left shifted by one) as an (unsigned) offset
  into the table.
  - Interpret the page table entry (PTE) at the resulting address as
specified below

The page table entry format is as follows:  

        FEDC BA98 7654 3210  
        FFFF FF.. DDDD ...V  
    
  - F - Page frame the PTE targets; top six bits of the target address.
The remainder of the physical address is retained from the requested
virtual address.
  - D - Device index used when mapping device memory into a process's
address space
  - V - Valid bit defines whether a PTE is valid. Interrupt if an
invalid entry is read.

## Boot Sequence

At power on, all memory and registers will be assumed to have zero values
throughout. If the underlying hardware does not ensure this then an
implementation must ensure that this is the case invisibly to the user (does not
interfere with user code). Atop that, the processor
initializes to this state:

  - RTC interrupts disabled
  - PS contains 0b1000 0011 0000 0000 (separated into nibbles for readability)
  - AT contains 0x090
  - PC is set to the value of the Reset field of the IVT

## Real Time Clock

The system includes an integrated real time clock (RTC) that keeps "wall clock"
time in a nonvolatile manner. This simplistic device tracks seconds since epoch
(01-01-1970 00:00:00 UTC) and provides six microsecond-interval interrupting
timers. Usage is through the RTC's mapped memory region:

| Address       | Purpose                                               |
| ------------- | ----------------------------------------------------- |
| 0x420-0x423   | 32-bit integer stating the seconds since epoch        |
| 0x424-0x42F   | Six 16-bit integers each specifying a number of microseconds before an interrupt should occur |

The timer registers are used for keeping time. When one is written to, its
value begins counting down as time passes. When one of these registers gets down
to zero its corresponding interrupt is raised. Keep this effect in mind as
repeating timers must be implemented through restarting timers rather than
left to the processor. [^cheating] Note that a timer will not raise an interrupt
before the specified time has elapsed but may not be able to activate promptly
afterward. That is, there may be some additional delay after time has elapsed.

[^cheating]: This is actually because it would make my code even uglier catching writes to the RTC register regions or such. Because I'm in charge of the spec I get to tweak it for convenience of implementation ;P It's not cheating when I get to write the rules *bwahahahaha*

## Dropping Privilege

It's important to be aware of privilege and minimize its potential for harm.
In the case of LC100, it is possible to switch from supervisor mode to user
mode by simply restoring any state (registers, particularly)
changed during supervisor mode activities, then:  

  - If inside an interrupt, RTI will handle the return to user mode including
  restoring PC and restoring PS from ISR (assure that ISR does hold the
  previous/correct value of PS!)  
  - Otherwise, set ISR directly to the desired value for PS and IPC to the
  address of the target user-mode code

# Instruction Set

Here's a nice list of opcodes. Isn't that nice?[^opkang]

| Low octet | Mnemonic| Priv? | Bit Pattern | Description             | Cycles |
| --------- | ------- | ----- | ------- | --------------------------- | ------ |
| 0000 000. |   MOV   |       |   G1    | R0 = R1                         | 1  |
| 0000 001. |   NEG   |       |   G1    | R0 = 0 - R0 (two's complement)  | 1  |
| 0000 010. |   ADD   |       |   G1    | R0 = R0 + R1                    | 1  |
| 0000 011. |   SUB   |       |   G1    | R0 = R0 - R1                    | 1  |
| 0000 100. |   MUL   |       |   G1    | R0 = R0 * R1 (16x16=32b)        | 8  |
| 0000 101. |   MLS   |       |   G1    | R0 = R0 * R1 (signed 16x16=32b) | 8  |
| 0000 110. |   DIV   |       |   G1    | R0 = R0 / R1                    | 48 |
| 0000 111. |   DVS   |       |   G1    | R0 = R0 / R1 (signed)           | 48 |
| 0001 000. |   MOD   |       |   G1    | R0 = R0 % R1 (modulus)          | 48 |
| 0001 001. |   MDS   |       |   G1    | R0 = R0 % R1 (modulus, signed)  | 48 |
| 0001 010. |   AND   |       |   G1    | Bitwise and                     | 1  |
| 0001 011. |   OR    |       |   G1    | Bitwise or                      | 1  |
| 0001 100. |   XOR   |       |   G1    | Bitwise exclusive or            | 1  |
| 0001 101. |   NOT   |       |   G1    | Bitwise not                     | 1  |
| 0001 110. |   LOD   |       |   G1    | Load                            | 1  |
| 0001 111. |   STO   |       |   G1    | Store                           | 1  |
| 001. .... |   BNE   |       |   BNC   | Branch if not equal             | 1  |
| 010. .... |   BLT   |       |   BNC   | Branch if less than             | 1  |
| 011. .... |   BLTU  |       |   BNC   | Branch if less than (unsigned)  | 1  |
| 1000 0000 |   LSL   |       |   G2    | Logical Shift Left              | 4  |
| 1000 0001 |   LSR   |       |   G2    | Logical Shift Right             | 4  |
| 1000 0010 |   ASL   |       |   G2    | Arithmetic Shift left           | 4  |
| 1000 0011 |   ASR   |       |   G2    | Arithmetic Shift Right          | 4  |
| 1000 0100 |   ROL   |       |   G2    | Rotate Left                     | 2  |
| 1000 0101 |   ROR   |       |   G2    | Rotate Right                    | 2  |
| 1000 0110 |   BTX   |       |   G2    | Invert a selected bit           | 2  |
| 1000 0111 |   BTC   |       |   G2    | Set 0 a selected bit            | 2  |
| 1000 1000 |   BTS   |       |   G2    | Set 1 a selected bit            | 2  |
| 1000 1001 |   SET   |   X   |   G2    | Set PS register                 | 1  |
| 1000 1010 |   GET   |       |   G2    | Get PS register                 | 1  |
| 1000 1011 |   SLP   |   X   |   G2    | Sleep until interrupt           | x  |
| 1000 1100 |   RTI   |   X   |   G2    | Return from interrupt           | 1  |
| 1000 1101 |   SWI   |       |   G2    | Raise software interrupt        | 1  |
| 1010 0000 |   JMP   |       |   JMP   | Jump                            | 1  |
| 1010 0001 |   JAL   |       |   JMP   | Jump and set return address     | 1  |
| 110. .... |   ADI   |       |   IMM   | Increment by value              | 1  |
| 111. .... |   SBI   |       |   IMM   | Decrement by value              | 1  |

[^opkang]: I may have largely kanged it off of the YCPU one, cut a few instructions, and rearranged the rest a little. Okay, it's fairly different now... but I still want to say "kang" in here somewhere so there you go :P

## Bit Patterns

Each instruction is written using a particular pattern of bits/format that
encodes all necessary information to perform the requested operation.
These patterns are:  

  - G1 pattern:  

        FEDC BA98 7654 3210  
        AAAA BBBB OOOO OOOe  
      
    - A - First operand register and the destination for the result
    - B - Second operand register (4-bit integer for shift ops)
    - e - Eight-bit mode flag. Set high for 8b operation.
    - O - Opcode  
  - G2 pattern:  

        FEDC BA98 7654 3210  
        AAAA BBBB OOOO OOOO  
      
    - A - First operand register and the destination for the result where
    appropriate
    - B - 4-bit integer
    - O - Opcode  
  - JMP pattern:  

        FEDC BA98 7654 3210  
        AAAA BBBB OOOO OOOO  
        
    - A field targets the register containing the address to jump to
    - B field targets the register to receive the return address and is
    unused by JMP
    - O - Opcode
  - BNC pattern:  

        FEDC BA98 7654 3210  
        BIII IIII OOOA AABB  
      
    - I is a 7-bit field that's left shifted by one to form an eight-bit target
    address for branch instructions
    - A is a three-bit field indicating a temporary register (t0-t7) containing
    the first operand
    - B is as A for the second operand. Its lowest bit is at index 0xF (consider
    that in little-endian form the B field is contiguous)
    - O - Opcode
  - IMM pattern:  

        FEDC BA98 7654 3210  
        AAAA IIII OOOI IIII
    
    - A is the index of the register upon which to operate
    - I is a 9-bit field with its high five bits in indices 4-0 and low nibble
    in indices B-8. (Put in little-endian byte order and cut off the opcode and
    A field)
    - O - Opcode

## Instructions

### Maths instructions

LC100 probably has an ALU lying around somewhere, so it's okay to use it for
maths processing. It's even got signed and unsigned multiplication and division.
Also the load and store instructions are in here for some reason. Probably[^yep]
a holdover from basing this document off the YCPU spec.
They are used like so:

        XXX[.8] R0[, R1]  
    
  - XXX is the mnemonic for the intended instruction (such as "SUB" or "NOT")
  Append .8 for eight-bit mode.
  - R0 is a designation (r7, r8, t0, t1, etc.) for the register containing the
  first operand that will receive the result of the computation
  - R1 is a designation for the register containing the second operand, where
    appropriate.

These instructions are:

| Opcode | Formula   | Meaning                                                 |
| --- | ------------ | ------------------------------------------------------- |
| ADD | R0 = R0 + R1 | Adds R1 to R0                                           |
| AND | R0 = R0 & R1 | Bitwise and                                             |
| DIV | R0 = R0 / R1 | Unsigned division                                       |
| DVS | R0 = R0 / R1 | Signed division                                         |
| XOR | R0 = R0 ^ R1 | Bitwise exclusive or                                    |
| LOD | R0 = M       | Sets R equal to value of memory at a given location     |
| MDS | R0 = R0 % R1 | Signed modulus                                          |
| MLS | R0 = R0 * R1 | Signed multiply, 32-bit result: High 16 bits of result  |
|     |              | are stored in t0. If R0 is t0, t0 will equal the low    |
|     |              | 16 bits of the result and the high 16 bits are lost.    |
| MOD | R0 = R0 % R1 | Unsigned modulus                                        |
| MOV | R0 = R1      | Move (copy) R1 into R0                                  |
| MUL | R0 = R0 * R1 | Unsigned multiply. As with MLI, the high 16 bits of the |
|     |              | the result are stored in R0, and may be overwritten.    |
| NEG | R = 0 - R    | Bitwise negation                                        |
| NOT | R = !R       | Bitwise NOT                                             |
| OR  | R0 = R0 | R1 | Bitwise OR                                              |
| STO | M = R        | Sets a memory location's value to that of R             |
| SUB | R0 = R0 - R1 | Subtracts R1 from R0                                    |

These all use the G1 bit pattern. Binary operations use the A field for the
numeric designation (0-15 for r0-r15) of the left operand and the B field for
the other. Unary operations ignore the B field. All operations respect the e
field as described [above](#instruction-bit-width). STO uses the register
indicated by the A field ("A register") as the source and accesses memory
at the address in the register indicated by the B field ("B register"). LOD
reads its address from the B register and loads into the A register.

[^yep]: Yep, it's that.

### Increment instructions

Supported increment and decrement instructions are ADI and SBI, allowing
modification of registers by 9-bit integer literals. 

Format:

        XXX R, V  
    
  - XXX is the mnemonic ("ADI" or "SBI")
  - R is the register to be \*cremented
  - V is a numeric literal value by which to modify the specified register

| Opcode | Formula   | Meaning                                                 |
| --- | ------------ | ------------------------------------------------------- |
| ADI | R = R + V    | Adds V to R                                             |
| SBI | R = R - V    | Subtracts V from R[^shocking]                           |

These use the IMM pattern.

[^shocking]: Bet you didn't expect that one! :O

### Bit-shifting instructions

Something something bit-shifty stuff.

        XXX R, V
    
  - XXX is the mnemonic (should I really keep explaining this?)
  - R is the register whose value will be shifted
  - V is the value by which to shift the value in R

| Opcode | Meaning                                                             |
| ------ | ------------------------------------------------------------------- |
| LSL    | Logical Shift Left                                                  |
| LSR    | Logical Shift Right                                                 |
| ASL    | Arithmetic Shift left                                               |
| ASR    | Arithmetic Shift Right                                              |
| RNL    | Rotate Left                                                         |
| RNR    | Rotate Right                                                        |

These all use the G2 bit pattern.

### Bit-testing instructions

Standardized testing for kids may be a component of a screwed-up system but it's
okay to test bits. We'll do that with these instructions

        XXX R, V
    
  - XXX is the mnemonic ("BTX," "BTC," "BTS")
  - R is the register to be tested
  - V is a numeric literal value selecting the bit index to test

| Opcode | Meaning                                                             |
| ------ | ------------------------------------------------------------------- |
| BTX    | Given a register and an index, invert the bit at that index         |
| BTC    | Given a register and an index, set 0 the bit at that index          |
| BTS    | Given a register and an index, set 1 the bit at that index          |

These use the G2 bit pattern. Note that no known vendor accepts this form
of BTC as currency.

### Branch instructions

Branch instructions compare the values of two registers and, if their
condition is met, update PC based on a given PC-relative address.

        XXX R0, R1, V
    
  - XXX is the mnemonic
  - R0 is the t-register of the first operand (t registers only)
  - R1 is the t-register of the second operand (t registers only)
  - V is a signed 7-bit integer that will be left shifted by one to form the
    branch target address

| Opcode | Meaning                                                             |
| ------ | ------------------------------------------------------------------- |
| BNE    | Branch if not equal (R0 ≠ R1)                                       |
| BLT    | Branch if less than (R0 < R1)                                       |
| BLTU   | Branch if less than (unsigned)                                      |

These instructions use the BNC pattern. **Important note** It was kindof a
struggle to cram all the stuff for these into sixteen bits so do note that the
available registers are *only* the temeporary "t" registers (t0-t7).

### Jump instructions

Jump instructions are a fun way to send the LC100 to distant places in memory!
Simply point one at a register and off it goes! Note that JAL accepts also a
register to write a return address into. If that place ends up being boring
you could then return by simply JMPing to the value of that register.

        XXX R0[, R1]
    
  - XXX is the mnemonic
  - R0 is the register containing the target address
  - R1 is the register to receive the return address (only applies to JAL)

| Opcode | Meaning                                                             |
| ------ | ------------------------------------------------------------------- |
| JMP    | Jump                                                                |
| JAL    | Jump and set return address                                         |

These instructions use the JMP pattern.

### Processor and Utility Instructions

These functions mostly involve affecting the state information of the processor
and so, other than SWI, require supervisor mode to be active else they'll raise
interrupts. Note that only SET and GET take arguments.

        XXX [R, PCR]
    
  - XXX is the mnemonic
  - R is the register containing the new/retrieved value (SET/GET only)
  - PCR is the register to be replaced/retrieved (SET/GET only)

| Opcode | Meaning                                                             |
| ------ | ------------------------------------------------------------------- |
| SET    | Set the value of a processor control register                       |
| GET    | Get the value of a processor control register                       |
| SLP    | Sleep until interrupt                                               |
| RTI    | Return from interrupt                                               |
| SWI    | Raise software interrupt                                            |

These instructions use the G2 pattern. GET/SET uses the contents of the B
register to replace the register specified by the A field like so:

| Value  | Register     |
| ------ | ------------ |
| 0      | PC           |
| 1      | PS           |
| 2      | AT           |
| 3      | IRPC         |
| 4      | ISR          |
| 5+     | (UndefFault) |
