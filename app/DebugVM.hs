{-# LANGUAGE BangPatterns #-}
module DebugVM where
import Control.Lens
import Control.Monad.State.Lazy
import Control.Monad.Except
import Control.Concurrent
import Control.Concurrent.STM
import Data.ByteString.Lazy as BSL hiding (null,head,tail,length,putStrLn,reverse)
import Data.Binary.Get
import Data.Either
import Data.Time.Clock.System
import Data.Word
import Data.Maybe (isJust, fromJust)
import Data.Binary hiding (put)
import GHC.Clock
import Graphics.UI.Gtk hiding (backspace, Action, Clock)
import LC100.SystemState
import LC100.Instructions
import LC100.Misc
import LC100.BitPatterns
import LC100.VSD
import LC100.Memory
import LC100.Devices
import LC100.Decode

--TODO
-- -Step button (with number of cycles ior target cycle count?)

--Load init ROM from an LC100 virtual system description file
runDebugVMFromFile :: String -> IO ()
runDebugVMFromFile filename = do
  (ramSizePre,devList,rom) <- readVSD filename
  let ramSize = if ramSizePre == 65535 then 65536 else fromIntegral ramSizePre
  --Set state with RAM size, ROM contents
  let ss = initialSystemState { _memory = BSL.replicate ramSize 0, _romemory = rom }
  --Load devices
  ss' <- loadDevs devList ss
  --Loop exec and state-keeping (R0, HWInterrupt checking, whatever else)
  {-nanos <- getMonotonicTimeNSec
  let micros = nanos `div` 1000
  vmLoop ss' micros-}
  wdts <- startupUI
  ss'' <- atomically $ newTVar ss'
  uiLoop wdts ss''

uiLoop wdts s = do
  nanos <- getMonotonicTimeNSec
  let curTime = nanos `div` 1000
  stepBCon <- (stepButton wdts) `on` buttonActivated $ do
    liftIO (vmIter s curTime) --TODO update time
    s' <- atomically $ readTVar s
    updateWidgets wdts s' "0" --TODO update cycle number too
    print "Meep"
    --TODO update register display, maybe allow modification
    --TODO run-to-target-cycle
  print "Foo"
  mainGUI

--TODO fix up the timing
vmIter :: TVar SysState -> Word64 -> IO ()
vmIter tvS oldTime = do
  s <- atomically $ readTVar tvS
  time <- getSystemTime
  newTimeNS <- getMonotonicTimeNSec
  let newTime = newTimeNS `div` 1000 --Monotonic time in µs
  let pcv = s ^. registers . pc
  --Fetch
  b0 <- runExceptT $ readMem True s pcv
  b1 <- runExceptT $ readMem True s (pcv+1)
  let instrB = rights [b0,b1]
  let !delta = newTime - oldTime
  let preS = over registers (& r0 .~ 0) s
  --Let's throw around some random bangs and hope that keeps things ticking smoothly
  !s' <- updateRTC preS time (fromIntegral delta)
  --Update timers
  --Raise interrupt from first expiring timer
  --Set any others that would expire to 1µs
  (s'', timerRang) <- checkTimers s' [0x424,0x426,0x428,0x42A,0x42C,0x42E] delta Nothing
  hwir <- checkHWInterrupts s (s ^. devMem) 0
  (vmIter' s' s'' newTime hwir timerRang instrB) >>= atomically . writeTVar tvS 

vmIter' :: SysState -> SysState -> Word64 -> Maybe Word8 -> Maybe (Word16) -> [Word16] -> IO SysState
vmIter' s s' newTime hwir timerRang instrB
  --First check for hardware interrupts
  | isHWIEnabled s
    && notInterrupted
    && isJust hwir = do
      newS <- execStateT (raiseInterrupt s (HWInterrupt (fromJust hwir))) s
      --vmLoop newS newTime
      return newS
  --Next check for clock interrupts
  --PS.I should keep us from getting stuck on multiple timers
  | awake && notInterrupted && isJust timerRang = return s'
    {-do
      vmLoop s' newTime-}
  --Else process onward!
  | awake && length instrB == 2 = do --Execute in order
      let instr = decodeInstruction instrB --Decode
      s'' <- execStateT (execute instr) s' --Execute!
      --We'll increment PC unless something (jump, branch, interrupt)
      -- has already changed it
      let newS = if s'' ^. registers . pc == s' ^. registers . pc
            then (over registers (& pc +~ 2) s'')
            else s''
      --vmLoop newS newTime
      return newS
  | not awake = return s --threadDelay 1000000 >> vmLoop s newTime
  | otherwise = error "Whoopsie! I don't know what went wrong :3 Maybe failed to fetch instruction."
  where
    awake = (not.isSleeping) s
    notInterrupted = (not.isHandlingInterrupt) s

checkHWInterrupts :: (Integral a) =>
  SysState -> [(TVar ByteString, TVar ByteString)] -> a -> IO (Maybe a)
checkHWInterrupts s dm i
  | null dm = return Nothing
  | otherwise = do
      mem <- atomically $ readTVar ((head dm) ^. _1)
      --Interrupt value
      let iv = runGet getWord16le $ BSL.take 2 (BSL.drop 6 mem)
      if iv == 0 then checkHWInterrupts s (tail dm) (i+1)
        else return $ Just i

updateRTC :: SysState -> SystemTime -> Word64 -> IO SysState
updateRTC s time delta = do
  --Update seconds-since-epoch
  let sse = systemSeconds time
  --reverse needed for little-endian-ness :-\
  --Should it be reversed as words or as the 32-bit value it is?
  --Hmmmmm.
  let sseW8s = reverse . unpack . encode $ sse 
  let s' = s { _memory = s ^. memory
               & (ix 0x420) .~ (sseW8s !! 0)
               & (ix 0x421) .~ (sseW8s !! 1)
               & (ix 0x422) .~ (sseW8s !! 2)
               & (ix 0x423) .~ (sseW8s !! 3) }
  return s'  

--TODO don't zero out timers after the first one that triggers
checkTimers :: SysState -> [Word16] -> Word64 -> Maybe Word16 -> IO (SysState,Maybe Word16)
checkTimers s [] _ rang = return (s, rang)
checkTimers s timers delta rang = do
  oT <- runExceptT $ readMem False s tAddr
  case oT of
    Left _ -> error "What the crap, reading RTC reg failed"
    Right oldTime -> do
      let newTime = fromIntegral ((fromIntegral oldTime :: Word64) - delta)
      if oldTime == 0
        then checkTimers s (tail timers) delta rang
        else if delta >= fromIntegral oldTime
        then --Timer expires
               if isJust rang
               then updateTimerAndContinue newTime --Already rang a timer
               else do --First timer, dingdingding! Congratulations!
                 let rang' = Just tIx
                 s' <- execStateT (raiseInterrupt s (Clock $ fromIntegral tIx)) s
                 checkTimers s' (tail timers) delta rang'
        else updateTimerAndContinue newTime --Timer does not expire, update timerg
  where
    tAddr = Prelude.head timers
    tIx = (tAddr - 0x424) `div` 2
    updateTimerAndContinue newTime = do
      s' <- runExceptT $
        writeMem False s tAddr (unpack $ encode $ byteSwap16 newTime)
      case s' of
        Left _ -> error "What the crap, writing RTC reg failed"
        Right s' -> checkTimers s' (tail timers) delta rang

loadDevs devList ss
  | null devList = return ss
  | otherwise = execStateT (loadDevice (head devList) ss) ss

data Widgets = Wdgt {
  r0e :: Entry,
  r1e :: Entry,
  r2e :: Entry,
  r3e :: Entry,
  r4e :: Entry,
  r5e :: Entry,
  r6e :: Entry,
  r7e :: Entry,
  r8e :: Entry,
  r9e :: Entry,
  r10e :: Entry,
  r11e :: Entry,
  r12e :: Entry,
  r13e :: Entry,
  r14e :: Entry,
  r15e :: Entry,
  pce :: Entry,
  pse :: Entry,
  ate :: Entry,
  irpce :: Entry,
  isre :: Entry,
  dumpMem :: Button,
  cycleNum :: Entry,
  targetCycle :: Entry,
  stepButton :: Button,
  runToTgtCycle :: Button }

updateWidgets ws s n = do
  entrySetText (r0e ws) (show $ s ^. registers . r0)
  entrySetText (r1e ws) (show $ s ^. registers . r1)
  entrySetText (r2e ws) (show $ s ^. registers . r2)
  entrySetText (r3e ws) (show $ s ^. registers . r3)
  entrySetText (r4e ws) (show $ s ^. registers . r4)
  entrySetText (r5e ws) (show $ s ^. registers . r5)
  entrySetText (r6e ws) (show $ s ^. registers . r6)
  entrySetText (r7e ws) (show $ s ^. registers . r7)
  entrySetText (r8e ws) (show $ s ^. registers . r8)
  entrySetText (r9e ws) (show $ s ^. registers . r9)
  entrySetText (r10e ws) (show $ s ^. registers . r10)
  entrySetText (r11e ws) (show $ s ^. registers . r11)
  entrySetText (r12e ws) (show $ s ^. registers . r12)
  entrySetText (r13e ws) (show $ s ^. registers . r13)
  entrySetText (r14e ws) (show $ s ^. registers . r14)
  entrySetText (r15e ws) (show $ s ^. registers . r15)
  entrySetText (pce ws) (show $ s ^. registers . pc)
  entrySetText (pse ws) (show $ s ^. registers . ps)
  entrySetText (ate ws) (show $ s ^. registers . atr)
  entrySetText (irpce ws) (show $ s ^. registers . irpc)
  entrySetText (isre ws) (show $ s ^. registers . isr)
  entrySetText (cycleNum ws) n

startupUI :: IO Widgets
startupUI = do
  void initGUI
  builder <- builderNew
  builderAddFromFile builder "app/DebuggyRunnerThingie.glade"

  mwin <- builderGetObject builder castToWindow "DRTMain"
  r0 <- builderGetObject builder castToEntry "r0Field"
  r1 <- builderGetObject builder castToEntry "r1Field"
  r2 <- builderGetObject builder castToEntry "r2Field"
  r3 <- builderGetObject builder castToEntry "r3Field"
  r4 <- builderGetObject builder castToEntry "r4Field"
  r5 <- builderGetObject builder castToEntry "r5Field"
  r6 <- builderGetObject builder castToEntry "r6Field"
  r7 <- builderGetObject builder castToEntry "r7Field"
  r8 <- builderGetObject builder castToEntry "r8Field"
  r9 <- builderGetObject builder castToEntry "r9Field"
  r10 <- builderGetObject builder castToEntry "r10Field"
  r11 <- builderGetObject builder castToEntry "r11Field"
  r12 <- builderGetObject builder castToEntry "r12Field"
  r13 <- builderGetObject builder castToEntry "r13Field"
  r14 <- builderGetObject builder castToEntry "r14Field"
  r15 <- builderGetObject builder castToEntry "r15Field"
  pc <- builderGetObject builder castToEntry "pcField"
  ps <- builderGetObject builder castToEntry "psField"
  at <- builderGetObject builder castToEntry "atField"
  irpc <- builderGetObject builder castToEntry "irpcField"
  isr <- builderGetObject builder castToEntry "isrField"
  cycCount <- builderGetObject builder castToEntry "cyclenum"
  cycTgt <- builderGetObject builder castToEntry "cycletarget"
  dumpMemBtn <- builderGetObject builder castToButton "dumpMemBtn"
  runToBtn <- builderGetObject builder castToButton "runToTargetCycle"
  stepBtn <- builderGetObject builder castToButton "stepButton"

  quitBtn <- builderGetObject builder castToMenuItem "quitBtn"
  quitBtn `on` menuItemActivated $ do
    liftIO mainQuit
    return ()
  widgetShowAll mwin
  mwin `on` deleteEvent $ do
    liftIO mainQuit
    return False
  return $ Wdgt r0 r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 r13 r14 r15 pc ps at irpc isr
    dumpMemBtn cycCount cycTgt stepBtn runToBtn
  --mainGUI
