module LC100 where
import LC100.SystemState
import LC100.Misc
import LC100.BitPatterns
import LC100.Instructions
import Control.Monad.State
import Control.Lens

testInstr instr = execStateT (execute instr)
