{-# LANGUAGE DuplicateRecordFields, TemplateHaskell,
    MultiParamTypeClasses, FlexibleInstances, FunctionalDependencies #-}
module LC100.BitPatterns where
import Control.Lens
import Data.Either
import Data.Word
import Data.Int
import LC100.Misc
import LC100.SystemState (SysState, RegState, LCFunc)
{-# ANN module "HLint: ignore Unused LANGUAGE pragma" #-}

class Delayed a where
  cycles :: a -> Int

--Instructions using the G1 bit pattern
data G1Op = NEG | ADD | SUB | MUL | DIV | MLS | MOV
  | DVS | MOD | MDS | AND | OR | XOR | NOT | LOD | STO deriving Show
instance Delayed G1Op where
  cycles MOV = 1
  cycles NEG = 1
  cycles ADD = 1
  cycles SUB = 1
  cycles MUL = 8
  cycles DIV = 48
  cycles MLS = 8
  cycles DVS = 48
  cycles MOD = 48
  cycles MDS = 48
  cycles AND = 1
  cycles OR = 1
  cycles XOR = 1
  cycles NOT = 1
  cycles LOD = 1
  cycles STO = 1
data G1Instr = G1Instr {
  _opcode        :: G1Op,
  _opd0          :: GPRegister,
  _opd1          :: GPRegister,
  _eightBitMode  :: Bool} deriving Show
mkFields ''G1Instr

--TODO Maybe add register versions of some of these?
--Instructions using the G2 bit pattern
data G2Op = LSL | LSR | ASL | ASR | ROL | ROR |
  BTX | BTC | BTS | GET | SET | SLP | RTI | SWI deriving Show
instance Delayed G2Op where
  cycles ASL = 4
  cycles ASR = 4
  cycles LSL = 4
  cycles LSR = 4
  cycles ROL = 2
  cycles ROR = 2
  cycles BTX = 2
  cycles BTC = 2
  cycles BTS = 2
  cycles GET = 1
  cycles SET = 1
  --Not exactly but we'll say it's the delay before
  --we can accept an interrupt, I guess
  cycles SLP = 1 
  cycles RTI = 1
  cycles SWI = 1
data G2Instr = G2Instr {
  _opcode        :: G2Op,
  _opd0          :: GPRegister,
  --It's not 16-bit but we'll handle that in decode
  --Also Word16 will be easier to work with at this level
  _opd1          :: Word16} deriving (Show)
mkFields ''G2Instr
  
--Instructions using the BNC bit pattern
data BNCOp = BNE | BLT | BLTU deriving Show
instance Delayed BNCOp where
  cycles _ = 1
data BNCInstr = BNCInstr {
  _opcode        :: BNCOp,
  --Branch ops only support t-registers for condition evaluation
  --This is because there just aren't enough bits in a fixed 16-bit-wide
  --format to do any better
  _opd0          :: TRegister,
  _opd1          :: TRegister,
  _offset        :: Int16} deriving (Show)
mkFields ''BNCInstr

--Instructions using the JMP bit pattern
data JMPOp = JMP | JAL deriving Show
instance Delayed JMPOp where
  cycles _ = 1
data JMPInstr = JMPInstr {
  _opcode        :: JMPOp,
  _tgtReg        :: GPRegister,
  _linkReg       :: GPRegister} deriving (Show)
mkFields ''JMPInstr

--Instructions using the IMM bit pattern
data IMMOp = ADI | SBI deriving Show
instance Delayed IMMOp where
  cycles _ = 1
data IMMInstr = IMMInstr {
  _opcode        :: IMMOp,
  _destReg       :: GPRegister,
  --Absolute value of the change to make
  --Whether to add or subtract is determined by the opcode
  _absVal        :: Word16} deriving (Show)
mkFields ''IMMInstr
