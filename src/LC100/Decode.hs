{-# LANGUAGE BinaryLiterals, ScopedTypeVariables #-}
module LC100.Decode where
import Data.ByteString.Lazy as BSL hiding (elem)
import Data.Bits
import Data.Word
import Data.Int
import Control.Monad.State.Lazy
import LC100.SystemState
import LC100.Instructions
import LC100.Misc
import LC100.BitPatterns

--This seems wrong somehow but whatever, I guess?
data InstWrapper =
  BNC BNCInstr |
  IMM IMMInstr |
  G1 G1Instr   |
  G2 G2Instr   |
  J JMPInstr deriving (Show)

instance Instruction InstWrapper where
  execute iw =
    case iw of
      BNC i -> execute i
      IMM i -> execute i
      G1 i -> execute i
      G2 i -> execute i
      J i -> execute i

--Must have at least the two bytes of the instruction to decode at the beginning
decodeInstrFromBS :: ByteString -> InstWrapper
decodeInstrFromBS word = decodeInstruction [b0,b1]
  where
    b0 = BSL.index word 0
    b1 = BSL.index word 1

--Must have at least the two bytes of the instruction to decode at the beginning
--What? Past Me, please explain.
decodeInstruction :: (Bits a, Num a, Integral a, Show a) => [a] -> InstWrapper
decodeInstruction [b0,b1]
  | firstThree `elem` [1,2,3] = decodeBNC firstThree b0 b1
  | firstThree `elem` [6,7] = decodeIMM firstThree b0 b1
  | firstSeven `elem` [0..15] = decodeG1 firstSeven b0 b1
  | firstEight `elem` [160,161] = decodeJMP firstEight b0 b1
  | firstEight `elem` [128..141] = decodeG2 firstEight b0 b1
  | otherwise = error ("decodeInstruction failed" ++ show b0 ++ ":" ++ show b1)
  where
    firstThree = flip shiftR 5 (b0 .&. 0b11100000)
    firstSeven = fromIntegral $ flip shiftR 1 (b0 .&. 0b11111110)
    firstEight = fromIntegral (b0 .&. 0b11111111)

decodeBNC opcode b0 b1 =
  case opcode of
    1 -> BNC $ BNCInstr BNE aReg bReg offs
    2 -> BNC $ BNCInstr BLT aReg bReg offs
    3 -> BNC $ BNCInstr BLTU aReg bReg offs
  where
    a = flip shiftR 2 (b0 .&. 0b00011100)
    b = (flip shiftL 1 (b0 .&. 0b00000011)) + if testBit b1 0xF then 1 else 0
    aReg = mapTRvalue a
    bReg = mapTRvalue b
    offs = fromIntegral $ flip shiftL 1 (fromIntegral (b1 .&. 0b01111111) :: Int8) :: Int16

decodeIMM opcode b0pre b1pre =
  case opcode of
    6 -> IMM $ IMMInstr ADI immAReg imm
    7 -> IMM $ IMMInstr SBI immAReg imm
  where
    b0 = fromIntegral b0pre :: Word16
    b1 = fromIntegral b1pre :: Word16
    imm = flip shiftL 4 (b0 .&. 0b00011111) + (b1 .&. 0b00001111)
    immA = flip shiftR 4 (b1 .&. 0b11110000)
    immAReg = mapGPvalue immA

decodeG1 opcode b0 b1 = G1 $ G1Instr (mnemmap !! opcode) a b ebm
  where
    a = mapGPvalue $ flip shiftR 4 (b1 .&. 0b11110000)
    b = mapGPvalue $ b1 .&. 0b00001111
    ebm = testBit b0 0
    mnemmap = [MOV,NEG,ADD,SUB,MUL,MLS,DIV,DVS,MOD,MDS,AND,OR,XOR,NOT,LOD,STO]

decodeJMP opcode b0 b1 =
  case opcode of
    160 -> J $ JMPInstr JMP a R0
    161 -> J $ JMPInstr JAL a b
  where
    a = mapGPvalue $ flip shiftR 4 (b1 .&. 0b11110000)
    b = mapGPvalue $ b1 .&. 0b1111

decodeG2 opcode b0 b1 = G2 $ G2Instr (mnemmap !! (opcode-128)) a b
  where
    a = mapGPvalue $ flip shiftR 4 (b1 .&. 0b11110000)
    b = fromIntegral $  b1 .&. 0b00001111
    mnemmap = [LSL,LSR,ASL,ASR,ROL,ROR,BTX,BTC,BTS,SET,GET,SLP,RTI,SWI]
