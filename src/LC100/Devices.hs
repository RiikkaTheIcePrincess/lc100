{-# LANGUAGE TemplateHaskell, BinaryLiterals #-}
module LC100.Devices where
--import System.Plugins.Load
import Control.Monad.State
import Control.Lens hiding ((|>))
import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.Chan
import qualified Data.ByteString.Lazy as BSL
import Data.ByteString.Lazy (ByteString)--,(|>))
import Data.Word
import Data.Bits

import LC100.SystemState
import LC100.Devices.Test
import LC100.Devices.HDD1

--TODO figure out a way to load devices in from a file or something somehow

loadDevice :: String -> SysState -> LCFunc
loadDevice name state = do
  let entryPt = case name of
                  "Test" -> testEntry
                  "hdd1" -> hdd1Entry
  e <- liftIO $ setupDevice entryPt state
  put e

{-
loadDevice :: String -> SysState -> LCFunc
loadDevice fileName state = do
  entryPre <- liftIO $ load_ ("devices/"++fileName++".o") [] "entry"
  case entryPre of
    LoadFailure m -> error $ show m
    LoadSuccess _ f -> do
      s <- liftIO $ setupDevice f state
      put s
-}
setupDevice :: (Chan (TVar ByteString) -> IO ()) -> SysState -> IO SysState
setupDevice entryPt state = do
  let devIx = length $ state ^. devMem
  --silo <- atomically $ newTVar (BSL.replicate 64 0)
  chan <- newChan :: IO (Chan (TVar ByteString))
  forkIO (entryPt chan)
  --Device must immediately respond with TVars to a 64B SILO region...
  silo <- readChan chan
  -- ...and a memory region of any length from 0 to 2^16 bytes, inclusive
  dMem <- readChan chan
  let newDev = (silo,dMem)
  return $ over devMem (++ [newDev]) state
