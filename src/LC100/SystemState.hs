{-# LANGUAGE TemplateHaskell, BinaryLiterals #-}
module LC100.SystemState where
import Control.Monad.State
import Control.Lens
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Data.ByteString.Lazy as BSL
import Data.Word
import Data.Bits

data RegState = RegState {
                      --General-purpose registers
                      --Also known as zero, ra, bp, sp, a0-a3, t0-t7
                        _r0,_r1,_r2,_r3,_r4,_r5,_r6,_r7,
                          _r8,_r9,_r10,_r11,_r12,_r13,_r14,_r15:: Word16
                      --Processor control registers
                      , _pc :: Word16   --Program Counter
                      , _ps :: Word16   --Processor Status register
                      , _atr :: Word16  --Address translation configuration register
                      , _irpc :: Word16 --Interrupt return address
                      , _isr :: Word16  --Interrupt scratch register
                      } deriving (Eq, Show)
makeLenses ''RegState

data SysState = SysState  {
                        _registers :: RegState
                      , _memory :: ByteString
                      , _romemory :: ByteString
                      --devMem is list of (SILO,main memory)
                      , _devMem :: [(TVar ByteString, TVar ByteString)]
                      } deriving Eq
instance Show SysState where
  show (SysState reg ram rom dMem) =
    "\nRegs: " ++ show reg
    ++ "\nram: (big list)"-- ++ show ram
makeLenses ''SysState

isMMUEnabled state = testBit psReg 0xE
  where psReg = state ^. registers . ps
isSModeEnabled state = testBit psReg 0xF
  where psReg = state ^. registers . ps
isSleeping state = testBit psReg 0xA
  where psReg = state ^. registers . ps
isHWIEnabled state = testBit psReg 0xD
  where psReg = state ^. registers . ps
isHandlingInterrupt state = testBit psReg 0xC
  where psReg = state ^. registers . ps

rs0 = RegState 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0b1000001100000000 0x090 0 0
initialSystemState = SysState rs0 (BSL.replicate (2^16) 0) (BSL.replicate 64 0) []

--TemplateHaskell likes to make things irritating so this is down here now (:
type LCFunc = StateT SysState IO ()

data Interrupt =
  Reset
  | Clock Word8
  | DivZeroFault
  | DoubleFault
  | UnprivFault
  | UndefFault
  | HWInterrupt Word8
  | BusRefresh
  | SWInterrupt
  | MemFault Word8
  deriving (Show,Eq)
