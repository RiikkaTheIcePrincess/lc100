{-# LANGUAGE FlexibleContexts, BinaryLiterals, LambdaCase #-}
module LC100.Instructions where
import Control.Concurrent (threadDelay) -- threadDelay µs
import Control.Lens hiding (cons)
import Control.Monad.State
import Control.Monad.Except
import Data.Binary (encode, byteSwap16, decode)
import Data.Binary.Get
import Data.Bits
import Data.ByteString.Lazy as BSL (cons, append, drop, take, unpack)
import Data.Either
import Data.Maybe
import Data.Word (Word16, Word8, Word32)
import Data.Int  (Int16,  Int8, Int32)
import LC100.BitPatterns
import LC100.Misc
import LC100.SystemState
import LC100.Memory

class Instruction a where
  execute :: a -> LCFunc

instance Instruction G1Instr where
  execute inst = do
    state <- get
    --Source/destination register
    let dest = inst ^. opd0
    let ebm = inst ^. eightBitMode
    let op1 = state ^. registers . mapGP dest
    let op2reg = inst ^. opd1
    let op2 = state ^. registers . mapGP op2reg
    --Cycle delay TODO FIXME etc.
    --lift $ threadDelay $ cycles (inst ^. opcode)*ceiling (1000*cycleDuration)
    case inst ^. opcode of
      MOV -> doMOV ebm dest op2 state
      NEG -> doNEG ebm op1 dest state
      ADD -> doADD ebm op1 op2 dest state
      SUB -> doSUB ebm op1 op2 dest state
      MUL -> doMUL ebm op1 op2 dest state
      DIV -> doDIV ebm op1 op2 dest state
      MLS -> doMLS ebm op1 op2 dest state
      DVS -> doDVS ebm op1 op2 dest state
      MOD -> doMOD ebm op1 op2 dest state
      MDS -> doMDS ebm op1 op2 dest state
      AND -> doAND ebm op1 op2 dest state
      OR  -> doORR ebm op1 op2 dest state
      XOR -> doXOR ebm op1 op2 dest state
      NOT -> doNOT ebm op1 dest state
      --LOD reads from address in B and writes to A
      LOD -> doLOD ebm op2 dest state
      --STO reads from A and writes to address in B
      STO -> doSTO ebm op2 op1 state

doMOV ebm dest op2 state
  | ebm = put (updateGPRegister state dest (op2 .&. 0xFF))
  | otherwise = put (updateGPRegister state dest op2)

--0x8000 == 0b1000000000000000
--0x80 == 0b10000000
--These are max negative value and cannot be negated in twos complement
doNEG ebm op1 dest state
  | ebm =
    if (op1 .&. 0xFF) == 0x80
    then --Can't negate this value
      put state
    else --8-bit, not 0x80/0b10000000
      let op1' = 0xFF .&. negate op1
      --let op1' = negate $ w16ToInt8 op1
      in put (updateGPRegister state dest op1') --(int8ToW16 op1'))
      --in put (set' (registers . mapGP dest) (int8ToW16 op1') state)
  | otherwise =
    if op1 == 0x8000
    then --Can't negate this either
      put state
    else --16-bit, not 0x8000
      let op1' = negate op1 --  $ w16ToInt16 op1
      in put (updateGPRegister state dest op1')  --(int16ToW16 op1'))

addSub :: Bool -> Bool -> Word16 -> Word16 -> GPRegister -> SysState -> LCFunc
addSub add ebm op1 op2 dest state
  | ebm =
    let op1U = w16ToW8 op1
        op1S = w16ToInt8 op1
        op2U = w16ToW8 op2
        op2S = w16ToInt8 op2
        sumU = (if add then (+) else (-)) op1U op2U
    in put (updateGPRegister state dest (w8ToW16 sumU))
  | otherwise =
    let op1S = w16ToInt16 op1
        op2S = w16ToInt16 op2
        sumU = (if add then (+) else (-)) op1 op2
    in put (updateGPRegister state dest sumU)

doADD = addSub True
doSUB = addSub False

doMUL ebm op1 op2 dest state
  | ebm =
    let lil1 = op1 .&. 0x00FF
        lil2 = op2 .&. 0x00FF
        lilProd = lil1 * lil2
        hiByte = lilProd .&. 0xFF00 --Get high byte
        loByte = lilProd .&. 0x00FF --Get low byte
        state' = updateGPRegister state R8 hiByte
    in put (updateGPRegister state' dest loByte)
  | otherwise =
    let prod = (fromIntegral op1) * (fromIntegral op2 :: Word32)
        --These will trim out the high and low words we need
        hiWord = w32ToW16hi prod
        loWord = w32ToW16lo prod
        state' = updateGPRegister state R8 hiWord
    in put (updateGPRegister state' dest loWord)

doDIV ebm op1 op2 dest state =
  if op2 == 0
  then raiseInterrupt state DivZeroFault
  else doDIV' ebm op1 op2 dest state
doDIV' ebm op1 op2 dest state
  | ebm =
    let lil1 = op1 .&. 0x00FF
        lil2 = op2 .&. 0x00FF
        lilQuot = lil1 `div` lil2
    in put (updateGPRegister state dest lilQuot)
  | otherwise =
    let quot = op1 `div` op2
    in put (updateGPRegister state dest quot)

doMLS ebm op1 op2 dest state
  | ebm =
    let lil1 = fromIntegral (w16ToInt8 op1) :: Int16
        lil2 = fromIntegral (w16ToInt8 op2) :: Int16
        prod = lil1 * lil2
        loByte = runGet getInt8 $ BSL.drop 1 $ encode prod
        hiByte = runGet getInt8 $ BSL.take 1 $ encode prod
        state' = updateGPRegister state R8 (int8ToW16 hiByte)
    in put (updateGPRegister state' dest (int8ToW16 loByte))
  | otherwise =
    let lil1 = w16ToInt32 op1 :: Int32
        lil2 = w16ToInt32 op2 :: Int32
        prod = lil1 * lil2
        loWord = runGet getInt16be $ BSL.drop 2 $ encode prod
        hiWord = runGet getInt16be $ BSL.take 2 $ encode prod
        state' = updateGPRegister state R8 (int16ToW16 hiWord)
    in put (updateGPRegister state' dest (int16ToW16 loWord))

doDVS ebm op1 op2 dest state =
  if op2 == 0
  then raiseInterrupt state DivZeroFault
  else doDVS' ebm op1 op2 dest state
doDVS' ebm op1 op2 dest state
  | ebm =
    let lil1 = w16ToInt8 op1
        lil2 = w16ToInt8 op2
        res = lil1 `div` lil2
    in put (updateGPRegister state dest (int8ToW16 res))
  | otherwise =
    let lil1 = fromIntegral op1
        lil2 = fromIntegral op2
        res = lil1 `div` lil2
    in put (updateGPRegister state dest (int16ToW16 res))

doMOD ebm op1 op2 dest state =
  if op2 == 0
  then raiseInterrupt state DivZeroFault
  else doMOD' ebm op1 op2 dest state
doMOD' ebm op1 op2 dest state
  | ebm =
    let lil1 = op1 .&. 0x00FF
        lil2 = op2 .&. 0x00FF
        lilMod = lil1 `mod` lil2
    in put (updateGPRegister state dest lilMod)
  | otherwise =
    let bigMod = op1 `mod` op2
    in put (updateGPRegister state dest  bigMod)

doMDS ebm op1 op2 dest state =
  if op2 == 0
  then raiseInterrupt state DivZeroFault
  else doMDS' ebm op1 op2 dest state
doMDS' ebm op1 op2 dest state
  | ebm =
    let lil1 = w8ToInt8 $ w16ToW8 op1 :: Int8
        lil2 = w8ToInt8 $ w16ToW8 op2 :: Int8
        res = lil1 `mod` lil2
    in put (updateGPRegister state dest (int8ToW16 res))
  | otherwise =
    let lil1 = w16ToInt16 op1 :: Int16
        lil2 = w16ToInt16 op2 :: Int16
        res = lil1 `mod` lil2
    in put (updateGPRegister state dest (int16ToW16 res))

doAND ebm op1 op2 dest state
  | ebm =
    let lil1 = w16ToW8 op1
        lil2 = w16ToW8 op2
        res = lil1 .&. lil2
    in put (updateGPRegister state dest (w8ToW16 res))
  | otherwise =
    let res = op1 .&. op2
    in put (updateGPRegister state dest res)

doORR ebm op1 op2 dest state
  | ebm =
    let lil1 = w16ToW8 op1
        lil2 = w16ToW8 op2
        res = lil1 .|. lil2
    in put (updateGPRegister state dest (w8ToW16 res))
  | otherwise =
    let res = op1 .|. op2
    in put (updateGPRegister state dest res)

doXOR ebm op1 op2 dest state
  | ebm =
    let lil1 = w16ToW8 op1
        lil2 = w16ToW8 op2
        res = lil1 `xor` lil2
    in put (updateGPRegister state dest ((op1 `xor` op2) .&. 0xFF))
  | otherwise =
    let res = op1 `xor` op2
    in put (updateGPRegister state dest res)

doNOT ebm op1 dest state
  | ebm =
    let lil1 = w16ToW8 op1
        res = complement lil1
    in put (updateGPRegister state dest  (w8ToW16 res))
  | otherwise =
    let res = complement op1
    in put (updateGPRegister state dest res)

doLOD :: Bool -> Word16 -> GPRegister -> SysState -> LCFunc
doLOD ebm memDest dest state = do
  res <- liftIO $ runExceptT $ readMem ebm state memDest
  case res of --readMem ebm state memDest of
    Left irpt -> raiseInterrupt state irpt
    Right val -> put (updateGPRegister state dest val)

doSTO :: Bool -> Word16 -> Word16 -> SysState -> LCFunc
--doSTO ebm addr newVal state
doSTO ebm memDest val state = do
  res <- liftIO $ runExceptT $ writeMem ebm state memDest val'
  case res of -- writeMem ebm state memDest val' of
    Left irpt -> raiseInterrupt state irpt
    Right state -> put state
  --If ebm, we're only writing the low byte
  --encode(?) is using big-endian *eyeroll*
  where val' = unpack $ encode $ byteSwap16 val

instance Instruction G2Instr where
  execute inst = do
    state <- get
    let dest = inst ^. opd0 --Register in A field
    let op2 = fromIntegral $ inst ^. opd1 --Value in B field
    let val = state ^. registers . mapGP dest --Value in A's register
    case inst ^. opcode of
      --Shifts
      ASL -> doASL state dest val op2
      ASR -> doASR state dest val op2
      LSL -> doASL state dest val op2  --Equivalent to ASL
      LSR -> doLSR state dest val op2
      ROL -> doROL state dest val op2
      ROR -> doROR state dest val op2
      --Bit tests
      BTX -> doBTX state val op2 dest
      BTC -> doBTC state val op2 dest
      BTS -> doBTS state val op2 dest
      --Processor/utility functions
      GET -> doGET state op2 dest
      SET -> doSET state op2 dest
      SLP -> doSLP state
      RTI -> doRTI state
      SWI -> doSWI state

--Shifts
doASL state dest val dist =
  let bigVal = w16ToW32 val
      bigVal' = shiftL bigVal dist
      val' = w32ToW16lo bigVal'-- .&. 0xFFFF
  in put (updateGPRegister state dest val')

doASR state dest val dist =
  let preVal' = shiftR (w16ToInt16 val) (fromIntegral dist)
      val' = int16ToW16 preVal'
  in put (updateGPRegister state dest val')

doLSR state dest val dist =
  let val' = shiftR val (fromIntegral dist)
  in put (updateGPRegister state dest val')

doROL state dest val dist =
  let val' = rotateL val dist
  in put (updateGPRegister state dest val')
      
doROR state dest val dist =
  let val' = rotateR val dist
  in put (updateGPRegister state dest val')

--Bit tests
doBTX state val tgt dest =
  let val' = complementBit val (fromIntegral tgt)
  in put (updateGPRegister state dest val')

doBTC state val tgt dest =
  let val' = clearBit val (fromIntegral tgt)
  in put (updateGPRegister state dest val')

doBTS state val tgt dest =
  let val' = setBit val (fromIntegral tgt)
  in put (updateGPRegister state dest val')

--Processor/util functions
mapPCRi = \case
  0 -> PC
  1 -> PS
  2 -> AT
  3 -> IRPC
  4 -> ISR
  5 -> error "Attempt to map invalid PCR"

checkSMode state func
  | isSModeEnabled state = func
  | otherwise = raiseInterrupt state UnprivFault

doGET state op2 dest =
  let pcr = mapPCRi op2
      pcrVal = state ^. registers . mapPC pcr
  in put (updateGPRegister state dest pcrVal)

doSET state op2 dest =
  let pcr = mapPCRi op2
      newVal = state ^. registers . mapGP dest
  in checkSMode state $ put (updatePCRegister state pcr newVal)

doSLP state = checkSMode state $ put (updatePCRegister state PS newPS)
  where newPS = (state ^. registers . ps) .|. 0b0000010000000000

doRTI state =
  let oldPS = state ^. registers . isr
      oldPC = state ^. registers . irpc
      state' = updatePCRegister state PC oldPC
  in checkSMode state $ put (updatePCRegister state' PS oldPS)

doSWI state = raiseInterrupt state SWInterrupt

instance Instruction BNCInstr where
  execute inst = do
    state <- get
    --Offset <.< Conflicted with 'offset' in the record field
    let off = inst ^. offset
    let op0r = inst ^. opd0
    let op1r = inst ^. opd1
    let op0 = state ^. registers . mapTReg op0r
    let op1 = state ^. registers . mapTReg op1r
    case inst ^. opcode of
      BNE  -> branch (/=) state op0 op1 off
      BLT  -> branch (<) state (w16ToInt16 op0) (w16ToInt16 op1) off
      BLTU -> branch (<) state op0 op1 off

branch f state op0 op1 offset
  | f op0 op1 = put (updatePCRegister state PC newPC)
  | otherwise = put state
  where oldPC = state ^. registers . mapPC PC
        newPC = fromIntegral $ (fromIntegral oldPC) + offset

instance Instruction JMPInstr where
  execute inst = do
    state <- get
    let tgt = inst ^. tgtReg
    let tgtAddr = state ^. registers . mapGP tgt
    let link = inst ^. linkReg
    case inst ^. opcode of
      JMP -> jump state tgtAddr Nothing
      JAL -> jump state tgtAddr (Just link)

jump state tgtAddr linkReg = put (updatePCRegister state' PC tgtAddr)
  where state' = case linkReg of
          Nothing -> state
          Just r  -> updateGPRegister state r ((state ^. registers . mapPC PC)+2)

instance Instruction IMMInstr where
  execute inst = do
    state <- get
    let dest = inst ^. destReg
    let val = inst ^. absVal
    let prev = state ^. registers . mapGP dest
    case inst ^. opcode of
      ADI -> put (updateGPRegister state dest (prev+val))
      SBI -> put (updateGPRegister state dest (prev-val))
