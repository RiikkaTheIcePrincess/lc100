module LC100.VSD where
import Data.ByteString.Lazy as BSL hiding (filter,null)
import Data.ByteString.Lazy.UTF8
import Data.Binary.Get
import Data.Word

readVSD :: FilePath -> IO ((Word16,[String],ByteString))
readVSD filename = do
  file <- BSL.readFile filename
  if BSL.length file /= 1024 then error "VSD100 file length incorrect, must be 1024B"
    else return $ readVSDBS file

readVSDBS :: ByteString -> (Word16,[String],ByteString)
readVSDBS str = (ramSize,filter (not . null) devNames,rom)
  where
    ramSize = runGet getWord16le $ BSL.take 2 str
    devNames = toString . runGet getLazyByteStringNul <$>
               [BSL.take 30 (BSL.drop (2+ix*30) str) | ix <- [0..14]]
    rom = BSL.drop 0x3C0 str
