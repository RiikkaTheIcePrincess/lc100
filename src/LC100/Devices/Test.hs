module LC100.Devices.Test where
import Control.Lens
import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.Chan
import qualified Data.ByteString.Lazy as BSL
import Control.Monad

testEntry :: (Chan (TVar BSL.ByteString)) -> IO ()
testEntry chan = do
  Prelude.putStrLn "Loadificating LC100DevTest."
  silo <- atomically $ newTVar (BSL.replicate 64 0)
  dMem <- atomically $ newTVar (BSL.replicate 64 6)
  writeChan chan silo
  writeChan chan dMem
  Prelude.putStrLn "Loadified LC100DevTest!"
  runTest silo dMem (BSL.pack [0]) (BSL.pack [0])

runTest silo dMem oldSV oldDV = do
  threadDelay 1000000 --Loop every second
  --Check the first value in the SILO, increment by 1 if it's even
  sv <- atomically $ readTVar silo
  --atomically $ writeTVar silo sv'
  --Same for device memory
  dv <- atomically $ readTVar dMem
  --atomically $ writeTVar dMem dv'
  --if sv ^. singular (ix 0) == 1 then print sv else return ()
  when (dv /= oldDV) (putStrLn $ "DV changed to " ++ show dv)
  when (sv /= oldSV) (putStrLn $ "SV changed to " ++ show sv)
  runTest silo dMem sv dv
