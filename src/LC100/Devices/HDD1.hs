module LC100.Devices.HDD1 (hdd1Entry,alterFile) where
import Control.Lens
import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.Chan
import qualified Data.ByteString.Lazy as BSL
import Control.Monad
import Data.Binary
import Data.Binary.Get

--TODO
-- -Support other files?
-- -Support some config in general, disk size, filename, etc. In VSD?
-- -Handle seeking and setting current index
fileName = "devices/simpleHDD.hdd1"

hdd1Entry :: (Chan (TVar BSL.ByteString)) -> IO ()
hdd1Entry chan = do
  Prelude.putStrLn "Loadificating HDD1."
  silo <- atomically $ newTVar mkSilo
  dMem <- atomically $ newTVar (BSL.replicate 64 0)
  --Load virtual disk file here, read first sector into dMem
  file <- BSL.readFile fileName
  writeChan chan silo
  writeChan chan dMem
  Prelude.putStrLn "Loadified HDD1!"
  runHDD silo dMem file fileName

mkSilo :: BSL.ByteString
mkSilo = BSL.pack $ [ --Little endian!
  1,1, -- Vendor/source/whatever ID --62
  1,1, -- Device model ID --60
  0,0x20, -- 8KiB mappable memory --58
  0,0, -- No pending interrupt --56
  --Interrupt with 1 to write changes to disk
  --Interrupt with 0x2 to revert changes/reload from disk
  16,0, -- Number of 4KiB sectors available on disk --54
  --We'll set it as a 64k drive for now
  0,0, -- Desired index pointer, initially zero (indexed in 4KiB sectors) --52
  0,0] -- Current index pointer, set after HDD seeks to a new location
  ++ replicate 50 0
  --`BSL.append` (BSL.replicate 52 0)

runHDD siloTV dMemTV file fileName = do
  silo <- atomically $ readTVar siloTV
  dMem <- atomically $ readTVar dMemTV
  let irptv = runGet getWord16le $ BSL.take 2 $ BSL.drop 6 silo
  let ptr = fromIntegral $ runGet getWord16le $ BSL.take 2 $ BSL.drop 10 silo
  let altered = alterFile file dMem ptr
  --TODO reset interrupt field after catching one?
  newF <- case irptv of
    1 -> BSL.writeFile fileName altered >> return altered --Write virtual disk file
    2 -> BSL.readFile fileName --Reload `file` from virtual disk file
    otherwise -> return file --return $ alterFile file dMem ptr --Load dMem into `file` at appropriate location
  --threadDelay 10000
  runHDD siloTV dMemTV newF fileName

--file in memory -> data to write to it -> pointer into file (/2^12)
alterFile f dMem ptr = f'
  where
    i = ptr*4096
    (preF',restF) = BSL.splitAt i f
    restF' = BSL.append dMem (BSL.drop (BSL.length dMem) restF)
    f' = BSL.append preF' restF'
