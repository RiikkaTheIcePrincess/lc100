{-# LANGUAGE BangPatterns #-}
module LC100.VM where
import Control.Lens
import Control.Monad.State.Lazy
import Control.Monad.Except
import Control.Concurrent
import Control.Concurrent.STM
import Data.ByteString.Lazy as BSL hiding (null,head,tail,length,putStrLn,reverse)
import Data.Binary.Get
import Data.Either
import Data.Time.Clock.System
import Data.Word
import Data.Maybe (isJust, fromJust)
import Data.Binary hiding (put)
import GHC.Clock
import LC100.SystemState
import LC100.Instructions
import LC100.Misc
import LC100.BitPatterns
import LC100.VSD
import LC100.Memory
import LC100.Devices
import LC100.Decode

--Load init ROM from an LC100 virtual system description file
runVMFromFile :: String -> IO ()
runVMFromFile filename = do
  (ramSizePre,devList,rom) <- readVSD filename
  let ramSize = if ramSizePre == 65535 then 65536 else fromIntegral ramSizePre
  --Set state with RAM size, ROM contents
  let ss = initialSystemState { _memory = BSL.replicate ramSize 0, _romemory = rom }
  --Load devices
  ss' <- loadDevs devList ss
  --Loop exec and state-keeping (R0, HWInterrupt checking, whatever else)
  nanos <- getMonotonicTimeNSec
  let micros = nanos `div` 1000
  --let ss' = over memory (& (ix 0x425) .~ 0xFF) ss --TESTING DEBUG FIXME TODO BBQ ETC.
  vmLoop ss' micros

vmLoop :: SysState -> Word64 -> IO ()
vmLoop s oldTime = do
  --putStrLn $ "PC: " ++ show (s ^. registers . pc) --DEBUG
  time <- getSystemTime
  newTimeNS <- getMonotonicTimeNSec
  let newTime = newTimeNS `div` 1000 --Monotonic time in µs
  let pcv = s ^. registers . pc
  --Fetch
  b0 <- runExceptT $ readMem True s pcv
  b1 <- runExceptT $ readMem True s (pcv+1)
  let instrB = rights [b0,b1]
  let !delta = newTime - oldTime
  let preS = over registers (& r0 .~ 0) s
  --Let's throw around some random bangs and hope that keeps things ticking smoothly
  !s' <- updateRTC preS time (fromIntegral delta)
  --Update timers
  --Raise interrupt from first expiring timer
  --Set any others that would expire to 1µs
  (s'', timerRang) <- checkTimers s' [0x424,0x426,0x428,0x42A,0x42C,0x42E] delta Nothing
  hwir <- checkHWInterrupts s (s ^. devMem) 0
  vmLoop' s' s'' newTime hwir timerRang instrB

vmLoop' :: SysState -> SysState -> Word64 -> Maybe Word8 -> Maybe (Word16) -> [Word16] -> IO ()
vmLoop' s s' newTime hwir timerRang instrB
  --First check for hardware interrupts
  | isHWIEnabled s
    && notInterrupted
    && isJust hwir = do
      newS <- execStateT (raiseInterrupt s (HWInterrupt (fromJust hwir))) s
      vmLoop newS newTime
  --Next check for clock interrupts
  --PS.I should keep us from getting stuck on multiple timers
  | awake && notInterrupted && isJust timerRang = do
      vmLoop s' newTime
  --Else process onward!
  | awake && length instrB == 2 = do --Execute in order
      let instr = decodeInstruction instrB --Decode
      s'' <- execStateT (execute instr) s' --Execute!
      --We'll increment PC unless something (jump, branch, interrupt)
      -- has already changed it
      let newS = if s'' ^. registers . pc == s' ^. registers . pc
            then (over registers (& pc +~ 2) s'')
            else s''
      vmLoop newS newTime
  | not awake = threadDelay 1000000 >> vmLoop s newTime
  | otherwise = error "Whoopsie! I don't know what went wrong :3 Maybe failed to fetch instruction."
  where
    awake = (not.isSleeping) s
    notInterrupted = (not.isHandlingInterrupt) s

checkHWInterrupts :: (Integral a) =>
  SysState -> [(TVar ByteString, TVar ByteString)] -> a -> IO (Maybe a)
checkHWInterrupts s dm i
  | null dm = return Nothing
  | otherwise = do
      mem <- atomically $ readTVar ((head dm) ^. _1)
      --Interrupt value
      let iv = runGet getWord16le $ BSL.take 2 (BSL.drop 6 mem)
      if iv == 0 then checkHWInterrupts s (tail dm) (i+1)
        else return $ Just i

updateRTC :: SysState -> SystemTime -> Word64 -> IO SysState
updateRTC s time delta = do
  --Update seconds-since-epoch
  let sse = systemSeconds time
  --reverse needed for little-endian-ness :-\
  --Should it be reversed as words or as the 32-bit value it is?
  --Hmmmmm.
  let sseW8s = reverse . unpack . encode $ sse 
  let s' = s { _memory = s ^. memory
               & (ix 0x420) .~ (sseW8s !! 0)
               & (ix 0x421) .~ (sseW8s !! 1)
               & (ix 0x422) .~ (sseW8s !! 2)
               & (ix 0x423) .~ (sseW8s !! 3) }
  return s'  

--TODO don't zero out timers after the first one that triggers
checkTimers :: SysState -> [Word16] -> Word64 -> Maybe Word16 -> IO (SysState,Maybe Word16)
checkTimers s [] _ rang = return (s, rang)
checkTimers s timers delta rang = do
  oT <- runExceptT $ readMem False s tAddr
  case oT of
    Left _ -> error "What the crap, reading RTC reg failed"
    Right oldTime -> do
      let newTime = fromIntegral ((fromIntegral oldTime :: Word64) - delta)
      if oldTime == 0
        then checkTimers s (tail timers) delta rang
        else if delta >= fromIntegral oldTime
        then --Timer expires
               if isJust rang
               then updateTimerAndContinue newTime --Already rang a timer
               else do --First timer, dingdingding! Congratulations!
                 let rang' = Just tIx
                 s' <- execStateT (raiseInterrupt s (Clock $ fromIntegral tIx)) s
                 checkTimers s' (tail timers) delta rang'
        else updateTimerAndContinue newTime --Timer does not expire, update timerg
  where
    tAddr = Prelude.head timers
    tIx = (tAddr - 0x424) `div` 2
    updateTimerAndContinue newTime = do
      s' <- runExceptT $
        writeMem False s tAddr (unpack $ encode $ byteSwap16 newTime)
      case s' of
        Left _ -> error "What the crap, writing RTC reg failed"
        Right s' -> checkTimers s' (tail timers) delta rang

loadDevs devList ss
  | null devList = return ss
  | otherwise = execStateT (loadDevice (head devList) ss) ss

