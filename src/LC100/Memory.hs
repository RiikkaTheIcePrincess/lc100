{-# LANGUAGE BinaryLiterals, LambdaCase #-}
module LC100.Memory where
import Control.Lens hiding (cons)
import Control.Monad.State
import Control.Monad.Except
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Data.Bits
import Data.Word
import Data.Int
import qualified Data.ByteString.Lazy as BSL
import Data.Maybe
import Data.Either
import Data.Binary
import Data.Binary.Get
import LC100.Misc
import LC100.SystemState

--TODO make this less awful (again)

--type Interruptable b = Either (Interrupt ) b
type Interruptable a = ExceptT Interrupt IO a

--USE THIS ONE -- Main memory reading function
--Reads from virtual or physical memory as appropriate
readMem :: Bool -> SysState -> Word16 -> Interruptable Word16
readMem ebm state addr = do
  s <- withMem state addr readByte directReadMem
  if ebm
    then return $ w8ToW16 s
    else (withMem state (addr+1) readByte directReadMem)
         >>= return . (runGet getWord16le) . BSL.pack . (s :) . (\v -> [v])
--USE THIS ONE if the other one isn't helping you perform writes
--It won't, of course, because it's only for reading memory
--So if you want to write to memory, use this one.
--But use that other one for reading. This one won't do that.
writeMem :: Bool -> SysState -> Word16 -> [Word8] -> Interruptable SysState
writeMem ebm state addr val = do
  s <- withMem state addr (writeByte (head val)) (directWriteMem $ head val)
  if ebm then return s else writeMem True s (addr+1) (tail val)

withMem ::
  SysState -> Word16 ->
  (SysState -> Int -> Int64 -> Interruptable b) ->
  (SysState -> Word16 -> Interruptable b) ->
  Interruptable b
withMem state addr memFunc directMemFunc =
  if asValid then
    if (pagingEnabled || not (isSModeEnabled state)) then do
      (devID, pgID) <- getAddrFromPteAddr (state ^. memory) pteAddr
      let pBaseAddr = pgID * 2^10
      memFunc state (fromIntegral devID) (fromIntegral $ pBaseAddr+(addr .&. 0x3FF))
    else directMemFunc state addr
  else throwError $ MemFault 1
  where
    sMode = testBit (state ^. registers . ps) 0xE
    asid = flip shiftR 13 (state ^. registers . atr .&. 0xE000) -- First three bits
    asLimit = flip shiftR 8 $ state ^. registers . ps .&. 0x0300 --ASID must be under 2^n
    asValid = asid < (min 8 (2^asLimit)) --And also under 8
    rootAddr = flip shiftL 3 (state ^. registers . atr .&. 0x1FFF) --AT.P field
    ptAddr = rootAddr + asid*(2^7)
    pagingEnabled = (not (testBit (state ^. registers . ps) 0xF)) ||
      testBit (state ^. registers . ps) 0xE -- PS.P field
    --Offset (top six bits of addr) into page table
    pteAddr = fromIntegral $ ptAddr + 2*(flip shiftR 10 addr .&. 0xFC00)

--TODO lots of cleanup and dedup
--Read a byte from a specified address in the given memory, checking bounds
readByte :: SysState -> Int -> Int64 -> Interruptable Word8
readByte state devID addr
  | devID > 0 && length (state ^. devMem) >= devID = do
      v <- liftIO $ atomically $ readTVar dMem
      let v' = v :: BSL.ByteString
      return $ v ^. singular (ix addr)
  | isROM = return $ state ^. romemory . singular (ix addr)
  | isSILO && length (state ^. devMem) > (fromIntegral devIx) = do
      v <- liftIO $ atomically $ readTVar dSILO
      let v' = v :: BSL.ByteString -- ???
      return $ v ^. singular (ix (addr `mod` 64))
  | BSL.length (state ^. memory) > addr = return $ state ^. memory . singular (ix addr)
  | otherwise = throwError $ MemFault 0
  where
    dMem = state ^. devMem . singular (ix (devID-1)) . _2
    dSILO = state ^. devMem . singular (ix (fromIntegral devIx)) . _1
    --How many 64B intervals past the SILO base address
    devIx = (addr - 0x40) `div` 64
    isSILO = addr < 0x400 && addr > 0x39
    isROM = addr < 0x40

writeByte :: Word8 -> SysState -> Int -> Int64 -> Interruptable SysState
writeByte val state devID addr
  | devID > 0 && length (state ^. devMem) >= devID = do
      v <- liftIO $ atomically $ readTVar dMem
      let v' = v & (ix addr) .~ val :: BSL.ByteString --Why the fuck is this needed???
      liftIO $ atomically $ writeTVar dMem v'
      return state
  | isROM = return state --Silently discard writes to ROM
  | isSILO && length (state ^. devMem) > (fromIntegral devIx) = do
      v <- liftIO $ atomically $ readTVar dSILO
      let v' = v & (ix (addr `mod` 64)) .~ val :: BSL.ByteString --Why the fuck is this needed???
      liftIO $ atomically $ writeTVar dSILO v'
      return state
  | BSL.length (state ^. memory) > addr = return $ over memory (& (ix addr) .~ val) state
  | otherwise = throwError $ MemFault 0
  where
    dMem = state ^. devMem . singular (ix (devID-1)) . _2
    dSILO = state ^. devMem . singular (ix (fromIntegral devIx)) . _1
    --How many 64B intervals past the SILO base address
    devIx = (addr - 0x40) `div` 64
    isSILO = addr < 0x400 && addr > 0x39
    isROM = addr < 0x40
    
--Gets the device ID and physical address;
--Handles PTE validation (both valid bit and whether the PTE itself is within memory)
getAddrFromPteAddr :: BSL.ByteString -> Word16 -> Interruptable ((Word16,Word16))
getAddrFromPteAddr pMem pteAddr =
  if BSL.length pteA /= 2
  then throwError (MemFault 0)
  else if valid then return (devID,pgID) else throwError (MemFault 2)
  where
    pteA = BSL.take 2 $ BSL.drop (fromIntegral pteAddr) pMem
    pte = runGet getWord16le pteA
    pgID = flip shiftR 10 (0xFC00 .&. pte)
    devID = flip shiftR 4 (0x00F0 .&. pte)
    valid = testBit pte 0

directReadMem :: SysState -> Word16 -> Interruptable Word8
--We've skipped the paging system but still need to access SILOs and ROM
directReadMem state addr = readByte state 0 (fromIntegral addr)
  
directWriteMem :: Word8 -> SysState -> Word16 -> Interruptable SysState
directWriteMem val state addr = writeByte val state 0 (fromIntegral addr)
