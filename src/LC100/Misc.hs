{-# LANGUAGE BinaryLiterals #-}
module LC100.Misc where
import Control.Lens hiding (cons)
import Control.Monad.State
import Control.Exception
import Data.Bits
import Data.Word
import Data.Int
import Data.Ix
import Data.ByteString.Lazy as BSL (cons, append, drop, take)
import Data.ByteString.Lens
import Data.Binary (encode, byteSwap16, decode)
import Data.Binary.Get
import Data.Maybe
import LC100.SystemState

mkFields = makeLensesWith classUnderscoreNoPrefixFields
--Duration of a CPU cycle in milliseconds
cycleDuration = 12.47

--zero,ra,bp,sp,a0-a3,t0-t7
data GPRegister = R0 | R1 | R2 | R3 | R4 | R5 | R6 | R7
  | R8 | R9 | R10 | R11 | R12 | R13 | R14 | R15  deriving (Show,Eq)
data PCRegister =  PC | PS | AT | IRPC | ISR deriving (Show,Eq)
data TRegister = T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 deriving (Show,Eq)

--R0 is `zero` the zero register. It is always zero.
updateGPRegister state reg newval
  | reg == R0 = state
  | otherwise = set' (registers . mapGP reg) newval state

updatePCRegister state reg newval = set' (registers . mapPC reg) newval state

{-
mapGPRvalue v
  | v < 16 = gpvMap !! v
  | otherwise = error "Impossible?? value attempted to map to GP register"
--gpvMap = [R0,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15]
-}
mapTRtoGP T0 = R8
mapTRtoGP T1 = R9
mapTRtoGP T2 = R10
mapTRtoGP T3 = R11
mapTRtoGP T4 = R12
mapTRtoGP T5 = R13
mapTRtoGP T6 = R14
mapTRtoGP T7 = R15

mapTRvalue 0 = T0
mapTRvalue 1 = T1
mapTRvalue 2 = T2
mapTRvalue 3 = T3
mapTRvalue 4 = T4
mapTRvalue 5 = T5
mapTRvalue 6 = T6
mapTRvalue 7 = T7

mapGPvalue 0 = R0
mapGPvalue 1 = R1
mapGPvalue 2 = R2
mapGPvalue 3 = R3
mapGPvalue 4 = R4
mapGPvalue 5 = R5
mapGPvalue 6 = R6
mapGPvalue 7 = R7
mapGPvalue 8 = R8
mapGPvalue 9 = R9
mapGPvalue 10 = R10
mapGPvalue 11 = R11
mapGPvalue 12 = R12
mapGPvalue 13 = R13
mapGPvalue 14 = R14
mapGPvalue 15 = R15
--

mapTReg T0 = r8
mapTReg T1 = r9
mapTReg T2 = r10
mapTReg T3 = r11
mapTReg T4 = r12
mapTReg T5 = r13
mapTReg T6 = r14
mapTReg T7 = r15

mapGP R0 = r0
mapGP R1 = r1
mapGP R2 = r2
mapGP R3 = r3
mapGP R4 = r4
mapGP R5 = r5
mapGP R6 = r6
mapGP R7 = r7
mapGP R8 = r8
mapGP R9 = r9
mapGP R10 = r10
mapGP R11 = r11
mapGP R12 = r12
mapGP R13 = r13
mapGP R14 = r14
mapGP R15 = r15

mapPC PC = pc
mapPC PS = ps
mapPC AT = atr
mapPC IRPC = irpc
mapPC ISR = isr

raiseInterrupt :: SysState -> Interrupt -> LCFunc
--Boot sequence thingie!
raiseInterrupt state Reset = do
  let wipeRTC = over memory (set (bytes.indices (inRange (4,7))) 0) state
  let setPS = set (registers . ps) 0b1010011100000000 wipeRTC
  let setAT = set (registers . atr) 0x90 setPS
  let s' = set (registers . pc) rstVec setAT
  put s'
  where
    mem = state ^. memory
    rstVec = runGet getWord16le $ BSL.take 2 (BSL.drop 0x400 mem)

raiseInterrupt state irpt
  | prvIrpt > 0 && prvIrpt /= 3 = raiseInterrupt' state DoubleFault
  | prvIrpt == 3 = raiseInterrupt state Reset --Triple fault
  | otherwise = raiseInterrupt' state irpt
  where prvIrpt = flip shiftR 4 $ state ^. registers . ps .&. 0x00F0

raiseInterrupt' :: SysState -> Interrupt -> LCFunc
raiseInterrupt' state irpt = do
  let afterISR = over registers (& isr .~ oldPS) state
  let afterUY = case irpt of
        --Reset -> afterISR
        Clock i -> setY i afterISR
        DivZeroFault -> setU afterISR
        DoubleFault -> setU afterISR
        UnprivFault -> setU afterISR
        UndefFault -> setU afterISR
        HWInterrupt i -> setY i afterISR
        BusRefresh -> afterISR
        SWInterrupt -> afterISR
        MemFault i -> setU (setY i afterISR)
  let afterIS = setIS afterUY (afterUY ^. registers . ps)
  let afterIPC = over registers (&irpc .~ oldPC) afterIS
  let afterPSV = setPSV afterIPC irptIx
  let afterPC = set (registers . pc) (0x400+(2*irptIx)) afterPSV
  put afterPC
  where
    oldPS = state ^. registers . ps
    oldPC = state ^. registers . pc
    setPS f s = over registers (& ps .~ f) s
    --Set only Y field
    setY i s = setPS ((oldPS .&. 0xFFF0) .|. ((fI i) .&. 0b1111)) s
    setU s = setPS ((if uMode s then setBit else clearBit) oldPS 0xB) s
    uMode = not.isSModeEnabled
    setIS s p = setPS (setBit (setBit p 0xF) 0xC) s
    --Set only V field
    setPSV s v = setPS (shiftL v 4 .|. (0xFF0F .&. (s ^. registers . ps))) s
    irptIx = interruptIndex irpt
    fI = fromIntegral

interruptIndex irpt = case irpt of
        Reset -> 0
        Clock i -> 1
        DivZeroFault -> 2
        DoubleFault -> 3
        UnprivFault -> 4
        UndefFault -> 5
        HWInterrupt i -> 6
        BusRefresh -> 7
        SWInterrupt -> 8
        MemFault i -> 9

--Also figure out how and why endianness is the most confusing thing in the world
--See w16ToW8: to get the low byte we have to swap bytes.
w16ToInt16 :: Word16 -> Int16
w16ToInt16 = fromIntegral
w16ToInt32 :: Word16 -> Int32
w16ToInt32 = fromIntegral . w16ToInt16
w16ToW8 :: Word16 -> Word8
w16ToW8 w = runGet getWord8 $ encode $ byteSwap16 w
--Swap bytes because we want the low bits, not the high ones
w16ToInt8 :: Word16 -> Int8
w16ToInt8 w = runGet getInt8 $ encode $ byteSwap16 w
w8ToW16 :: Word8 -> Word16
w8ToW16 w = runGet getWord16be ((0 :: Word8) `cons` encode w)
w8ToInt8 :: Word8 -> Int8
w8ToInt8 = fromIntegral
zo = encode (0 :: Word8)
w8ToW32 :: Word8 -> Word32
w8ToW32 w = runGet getWord32be $ zo `append` zo `append` zo `append` encode w
w16ToW32 :: Word16 -> Word32
w16ToW32 w = runGet getWord32be $ zo `append` zo `append` encode w
w32ToW16lo :: Word32 -> Word16
w32ToW16lo w = runGet getWord16be $ BSL.drop 2 $ encode w
w32ToW16hi :: Word32 -> Word16
w32ToW16hi w = runGet getWord16be $ BSL.take 2 $ encode w

int8ToW16 :: Int8 -> Word16
int8ToW16 w = runGet getWord16be ((0 :: Word8) `cons` encode w) :: Word16
int8ToW8 :: Int8 -> Word8
int8ToW8 w = runGet getWord8 $ encode w :: Word8
int16ToW16 :: Int16 -> Word16
int16ToW16 w = runGet getWord16be $ encode w
int32ToW16lo :: Int32 -> Word16
int32ToW16lo w = runGet getWord16be $ BSL.drop 2 $ encode w
int32ToW16hi :: Int32 -> Word16
int32ToW16hi w = runGet getWord16be $ BSL.take 2 $ encode w
